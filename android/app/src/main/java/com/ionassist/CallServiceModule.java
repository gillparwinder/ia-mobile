package com.ionassist;

import android.widget.Toast;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import android.content.Context;
import android.os.Bundle;
import android.content.Intent;
import com.facebook.react.HeadlessJsTaskService;
import com.ionassist.service.CallService;

import java.util.Map;
import java.util.HashMap;

public class CallServiceModule extends ReactContextBaseJavaModule {
  private static ReactApplicationContext reactContext;

  private static final String DURATION_SHORT_KEY = "SHORT";
  private static final String DURATION_LONG_KEY = "LONG";
  private Context mContext;

  CallServiceModule(ReactApplicationContext context,Context cntxt) {
    super(context);
    reactContext = context;
    this.mContext = cntxt;
  }

  @Override
  public String getName() {
    return "CallService";
  }

  @Override
  public Map<String, Object> getConstants() {
    final Map<String, Object> constants = new HashMap<>();
    constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
    constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
    return constants;
  }

  @ReactMethod
  public void start(String entityId, String role, String username) {

     Intent service = new Intent(mContext, CallService.class);
     Bundle bundle = new Bundle();

     bundle.putString("role",role);
     bundle.putString("entityId",entityId);
     bundle.putString("username",username);
     service.putExtras(bundle);
     mContext.startService(service);
     HeadlessJsTaskService.acquireWakeLockNow(mContext);
  }

  @ReactMethod
  public void stop() {
    Intent service = new Intent(mContext, CallService.class);
     Bundle bundle = new Bundle();
     service.putExtras(bundle);
     mContext.stopService(service);
  }


}