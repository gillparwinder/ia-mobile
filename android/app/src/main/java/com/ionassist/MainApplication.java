package com.ionassist;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.rnfs.RNFSPackage;
import org.reactnative.camera.RNCameraPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import io.wazo.callkeep.RNCallKeepPackage;
import com.reactnativecommunity.rnpermissions.RNPermissionsPackage;
import fr.greweb.reactnativeviewshot.RNViewShotPackage;
import com.johnsonsu.rnsoundplayer.RNSoundPlayerPackage;

import com.corbt.keepawake.KCKeepAwakePackage;
import com.heanoria.library.reactnative.locationenabler.RNAndroidLocationEnablerPackage;
import io.invertase.firebase.RNFirebasePackage;
import com.syan.agora.AgoraPackage;
import com.github.yamill.orientation.OrientationPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.lugg.ReactNativeConfig.ReactNativeConfigPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.ionassist.CallServicePackage;
import com.ionassist.invokeapp.ReactNativeInvokeAppPackage;
import io.invertase.firebase.database.RNFirebaseDatabasePackage; 
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new RNFSPackage(),
            new RNCameraPackage(),
            new RNDeviceInfo(),
            new RNCallKeepPackage(),
            new RNPermissionsPackage(),
            new RNViewShotPackage(),
            new RNSoundPlayerPackage(),
            // new ReactNativeInvokeAppPackage(),
            new KCKeepAwakePackage(),
            new RNAndroidLocationEnablerPackage(),
            new RNFirebasePackage(),
            new RNFirebaseDatabasePackage(),
            new RNFirebaseMessagingPackage(),
            new RNFirebaseNotificationsPackage(),
            new AgoraPackage(),
            new OrientationPackage(),
            new VectorIconsPackage(),
            new RNGestureHandlerPackage(),
            new ReactNativeConfigPackage(),
            new CallServicePackage(getApplicationContext()),
            new ReactNativeInvokeAppPackage()
      );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
     SoLoader.init(this, /* native exopackage */ false); 
  }
}
