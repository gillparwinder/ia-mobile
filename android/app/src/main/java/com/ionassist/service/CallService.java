//imports
package com.ionassist.service;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.jstasks.HeadlessJsTaskConfig;
import javax.annotation.Nullable;
//creating the service class
public class CallService extends HeadlessJsTaskService {
    
    // @Override
    @Nullable
    protected HeadlessJsTaskConfig getTaskConfig(Intent intent) {
        Bundle extras = intent.getExtras();
        if(extras != null ){
            return new HeadlessJsTaskConfig(
                "NotifyIncomingCall", //JS function to call
                // extras != null ? Arguments.fromBundle(extras) : null,
                Arguments.fromBundle(extras),
                1000,
                true);
        }
        return null;
    }
}