// CallServicePackage.java

package com.ionassist;
import android.app.Application;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CallServicePackage implements ReactPackage {
   private Context context;
    CallServicePackage(Context context){
        this.context = context;
    }

  @Override
  public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
    return Collections.emptyList();
  }

  @Override
  public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
    List<NativeModule> modules = new ArrayList<>();

    modules.add(new CallServiceModule(reactContext,this.context));

    return modules;
  }

}