/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Provider } from "react-redux";
import AppContainer from "./src/feature/navigators/appNavigator";
import { PersistGate } from "redux-persist/lib/integration/react";
import configureStore from "./store";
import { AppRegistry, NativeEventEmitter, NativeModules, Platform } from 'react-native';
import { NavigationActions } from 'react-navigation'
import NotifyIncomingCall from './src/service/notifyIncomingCall'
import bgMessaging from './src/service/bgMessaging'

if (Platform.OS == 'android') {
  AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging)
  AppRegistry.registerHeadlessTask('NotifyIncomingCall', () => NotifyIncomingCall.bind(null, configureStore().store))
}

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      storeConfig: configureStore()
    };
  }

  navigator = null;
  componentWillMount() {
    if (Platform.OS == 'android') {
      const eventEmitter = new NativeEventEmitter(NativeModules.ReactNativeInvokeApp);
      eventEmitter.addListener('appInvoked', (data) => {
        console.log("Service listened the call and navigating to route");
        this.navigator &&
          this.navigator.dispatch(
            NavigationActions.navigate({
              routeName: data.route,
              params: {
                sessionId: data.sessionId,
                firebaseKey: data.firebaseKey,
                requestedUser: data.requestedUser,
                userId: data.userId,
                from: data.from
              }
            })
          );
      });
    }
  }

  render() {
    return (
      <Provider store={this.state.storeConfig.store}>
        <PersistGate persistor={this.state.storeConfig.persistor}>
          <AppContainer ref={nav => { this.navigator = nav; }} />
        </PersistGate>
      </Provider>
    );
  }
}
