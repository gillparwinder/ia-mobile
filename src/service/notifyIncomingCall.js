import firebase from "react-native-firebase";
import Config from "react-native-config";
import ReactNativeInvokeApp from '../feature/components/invokeApp'

const firebaseConfig = {
    apiKey: Config.API_KEY,
    authDomain: Config.AUTH_DOMAIN,
    databaseURL: Config.DATABASE_URL,
    projectId: Config.PROJECT_ID,
    storageBucket: Config.STORAGE_BUCKET,
    messagingSenderId: Config.MESSAGING_SENDER_ID,
    appId: Config.APP_ID
};
let sessionRef = "";

module.exports = async (store, data) => {
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }

    if (store.getState().loginReducer.role == "CENTRAL_SUPPORT_EXECUTIVE") {
        sessionRef = firebase
            .database()
            .ref("ionassist-root/sessions/" + store.getState().loginReducer.member_entity);

        sessionRef.on("child_added", snapshot => {
            if (snapshot.val().status === "WAITING" && snapshot.val().requested_user_name != store.getState().loginReducer.user_name) {
                console.log("New Call from driver initiated from service");
                if (snapshot.val().hasOwnProperty('ignored_users') && snapshot.val().ignored_users.includes(store.getState().loginReducer.user_id)) {
                    console.log("Call already disconnected by this user");
                    return false;
                }
                const routeObject = {
                    route: 'CallAlert',
                    requestedUser: snapshot.val().requested_user_name,
                    userId: snapshot.val().requested_user_id,
                    sessionId: snapshot.val().session_id,
                    firebaseKey: snapshot.val().firebase_key,
                    from: 'fieldExecutive'
                };
                if (store.getState().loginReducer.loggedIn && !store.getState().homeReducer.isCallActive) {
                    ReactNativeInvokeApp.invokeApp({
                        data: routeObject,
                    })
                }
            }
        });
    }
    else {
        sessionRef = firebase
            .database()
            .ref('ionassist-root/one_to_one/' + store.getState().loginReducer.member_entity)
        sessionRef.on('child_added', snapshot => {
            console.log("New Call from driver initiated from service");
            if (snapshot.val().status === 'WAITING' && snapshot.val().to === store.getState().loginReducer.user_name) {
                const routeObject = {
                    route: 'CallAlert',
                    requestedUser: snapshot.val().requested_user_name,
                    userId: snapshot.val().requested_user_id,
                    sessionId: snapshot.val().session_id,
                    firebaseKey: snapshot.val().firebase_key,
                    from: 'SupportExecutive'
                };

                if (store.getState().loginReducer.loggedIn && !store.getState().homeReducer.isCallActive) {
                    ReactNativeInvokeApp.invokeApp({
                        data: routeObject,
                    })
                }
            }
        })
    }
}
