import firebase from "react-native-firebase";
import Config from "react-native-config";
import ReactNativeInvokeApp from '../feature/components/invokeApp'

export default async (message) => {
    console.log("background message recieved firebase = " + message);
    const routeObject = {
        route: 'CallAlert',
        requestedUser: message.data.requestedUser,
        userId: message.data.userId,
        sessionId: message.data.sessionId,
        firebaseKey: message.data.firebaseKey,
        from: message.data.from_role == 'CENTRAL_SUPPORT_EXECUTIVE' ? 'SupportExecutive' : 'fieldExecutive'
    };
    ReactNativeInvokeApp.invokeApp({
        data: routeObject,
    })
    return Promise.resolve();
}
