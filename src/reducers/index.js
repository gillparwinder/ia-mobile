import { combineReducers, createStore, applyMiddleware } from "redux";

import homeReducer from "../feature/home/reducers/homeReducer";
import loginReducer from "../feature/login/reducers/loginReducers";
import { persistReducer } from 'redux-persist';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import storage from 'redux-persist/lib/storage';

const persistConfigLogin = {
  key: "login",
  storage: storage,
  // stateReconciler: autoMergeLevel2
};


const AppReducers = combineReducers({
  homeReducer: homeReducer,
  loginReducer: persistReducer(persistConfigLogin, loginReducer)
});

export default AppReducers
