import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions';
import { Platform, PermissionsAndroid } from 'react-native'

export const requestPermissions = () => {
    if (Platform.OS == 'ios') {
        request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then((result) => {
            console.log("LocationPermission:" + result);
        });

        request(PERMISSIONS.IOS.CAMERA).then((result) => {
            console.log("CameraPermission:" + result);
        });

        request(PERMISSIONS.IOS.MICROPHONE).then((result) => {
            console.log("LocationMicrophone:" + result);
        });
    }
    else {
        PermissionsAndroid.requestMultiple([
            PermissionsAndroid.PERMISSIONS.CAMERA,
            PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
        ])
    }
}
