import { Platform } from 'react-native'
import Config from "react-native-config";
import { getDeviceInformation } from './helper'
import firebase from "react-native-firebase";

export const firebaseConfig = {
    apiKey: Config.API_KEY,
    authDomain: Config.AUTH_DOMAIN,
    databaseURL: Config.DATABASE_URL,
    projectId: Config.PROJECT_ID,
    storageBucket: Config.STORAGE_BUCKET,
    messagingSenderId: Config.MESSAGING_SENDER_ID,
    appId: Config.APP_ID
};

export const requestFCM = (callback) => {
    if (Platform.OS == 'android') {
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }

        firebase.messaging().getToken()
            .then(fcmToken => {
                if (fcmToken) {
                    let deviceInfo = getDeviceInformation()
                    console.log("fcmToken= " + fcmToken);
                    callback('success', { 'deviceId': deviceInfo.device_uid, 'fcmToken': fcmToken });
                    // this.props.registerFCMToken(deviceInfo.device_uid, fcmToken);
                }
                else {
                    console.log("User doesnt have a token");
                    callback('error', { 'deviceId': '', 'fcmToken': '' });
                }
            });

        this.onTokenRefreshListener = firebase.messaging().onTokenRefresh(fcmToken => {
            console.log("OnRefreshtoken is called");
        });

        firebase.messaging().hasPermission()
            .then(enabled => {
                if (enabled) {
                    console.log("User has permission to firebase message.");
                }
                else {
                    firebase.messaging().requestPermission()
                        .then(() => {
                            // User has authorised  
                            console.log("User has granted Permission.")
                        })
                        .catch(error => {
                            // User has rejected permissions  
                            console.log("User has rejected permission")
                        });
                }
            })
    }
}