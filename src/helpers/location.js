import { PermissionsAndroid } from 'react-native';
import { check, request, PERMISSIONS, RESULTS } from 'react-native-permissions';

export const checkPermissions = async (permissionName, title, message) => {
    try {
        const granted = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS[permissionName.toUpperCase()]
        );
        if (granted) {
            return { permission: true, err: null };
        } else {
            const reqGranted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS[permissionName.toUpperCase()],
                { title: title, message: message }
            );
            if (reqGranted === PermissionsAndroid.RESULTS.GRANTED) {
                return { permission: true, err: null };
            } else {
                return { permission: false, err: 'permission Denied' };
            }
        }
    } catch (err) {
        console.warn(err);
    }
};

export const getGeoLocation = (options, cb) => {
    let highAccuracySuccess = false;
    let highAccuracyError = false;
    let highAccuracy =
        !options || options.highAccuracy === undefined
            ? true
            : options.highAccuracy;
    let timeout =
        !options || options.timeout === undefined ? 10000 : options.timeout;

    let getLowAccuracyPosition = () => {
        console.log('REQUESTING POSITION', 'HIGH ACCURACY FALSE');
        navigator.geolocation.getCurrentPosition(
            position => {
                console.log('POSITION NETWORK OK', position);
                cb(position.coords);
            },
            error => {
                console.log(error);
                cb(null, error);
            },
            {
                enableHighAccuracy: false,
                timeout: 10000,
                maxAge: 0,
            }
        );
    };

    if (highAccuracy) {
        console.log('REQUESTING POSITION', 'HIGH ACCURACY TRUE');
        const watchId = navigator.geolocation.watchPosition(
            position => {
                // location retrieved
                highAccuracySuccess = true;
                console.log('POSITION GPS OK', position);
                navigator.geolocation.clearWatch(watchId);
                cb(position.coords);
            },
            error => {
                console.log(error);
                highAccuracyError = true;
                navigator.geolocation.clearWatch(watchId);
                getLowAccuracyPosition();
            },
            {
                enableHighAccuracy: true,
                timeout: 20000,
                maxAge: 0,
                distanceFilter: 1,
            }
        );

        setTimeout(() => {
            if (!highAccuracySuccess && !highAccuracyError) {
                getLowAccuracyPosition();
            }
        }, timeout);
    }
};

export const requsestLocationAccessIos = () => {
    check(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE)
        .then((result) => {
            if (result == RESULTS.BLOCKED) {
                alert("Access to location is blocked. Please enable it.")
            }
            else if (result != RESULTS.GRANTED) {
                request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then((result) => {
                });
            }
        })
        .catch((error) => {
            request(PERMISSIONS.IOS.LOCATION_WHEN_IN_USE).then((result) => {
            });
        });
}