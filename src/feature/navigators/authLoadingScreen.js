import React, { Component } from "react";
import { View, StatusBar, ActivityIndicator } from "react-native";
import { connect } from "react-redux";

class AuthLoadingScreen extends Component {

    constructor(props) {
        super(props);
        this._bootstrapAsync();
    }

    _bootstrapAsync = async () => {

        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.

        this.props.navigation.navigate(this.props.loggedIn ? (this.props.role == 'CENTRAL_SUPPORT_EXECUTIVE' ? "CustomerApp" : "App") : "Auth");
    };

    render() {
        return (
            <View>
                <ActivityIndicator />
                <StatusBar barStyle="default" />
            </View>
        );
    }
}

const mapStateToProps = state => {
    return {
        loggedIn: state.loginReducer.loggedIn,
        role: state.loginReducer.role
    }
}

export default connect(mapStateToProps)(AuthLoadingScreen);
//export default AuthLoadingScreen;