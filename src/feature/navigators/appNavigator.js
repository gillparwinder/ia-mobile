import Index from '../home/pages/fieldExecutive/index'
import Login from './../login/pages/login'
import {
  createStackNavigator,
  createAppContainer,
  createSwitchNavigator
} from 'react-navigation'
import AuthLoadingScreen from './authLoadingScreen'
import PasswordReset from '../login/pages/passwordReset'
import ForgotPassword from '../login/pages/forgotPassword'
import Language from '../login/pages/language'
import MemberEntity from '../login/pages/memberEntity'
import AgoraComponent from '../home/pages/fieldExecutive/agora'
import AttendCallFromSupport from '../home/pages/fieldExecutive/attendCallFromSupport/attendCallFromSupport'

import ListFieldExecutives from '../home/pages/centralSupport/listFieldExecutives/listFieldExecutives'
import CallFieldExecutive from '../home/pages/centralSupport/callFieldExecutive/callFieldExecutive'
import ReceiveCall from '../home/pages/centralSupport/receiveCall/receiveCall'
import CallAlert from '../home/pages/callAlert/callAlert'
import VideoRecord from '../home/pages/fieldExecutive/videoRecord/videoRecord'


const AuthNavigation = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'Login'
  }
)

const AppNavigation = createStackNavigator(
  {
    Home: {
      screen: Index,
      navigationOptions: {
        header: null
      }
    },
    Language: {
      screen: Language,
      navigationOptions: {
        header: null
      }
    },
    PasswordReset: {
      screen: PasswordReset,
      navigationOptions: {
        header: null
      }
    },
    MemberEntity: {
      screen: MemberEntity,
      navigationOptions: {
        header: null
      }
    },
    Agora: {
      screen: AgoraComponent,
      navigationOptions: {
        header: null
      }
    },
    AttendCallFromSupport: {
      screen: AttendCallFromSupport,
      navigationOptions: {
        header: null
      }
    },
    CallAlert: {
      screen: CallAlert,
      navigationOptions: {
        header: null
      }
    },
    VideoRecord: {
      screen: VideoRecord,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'Home'
  }
)

const CustomerAppNavigation = createStackNavigator(
  {
    Home: {
      screen: Index,
      navigationOptions: {
        header: null
      }
    },
    Language: {
      screen: Language,
      navigationOptions: {
        header: null
      }
    },
    PasswordReset: {
      screen: PasswordReset,
      navigationOptions: {
        header: null
      }
    },
    MemberEntity: {
      screen: MemberEntity,
      navigationOptions: {
        header: null
      }
    },
    ListFieldExecutives: {
      screen: ListFieldExecutives,
      navigationOptions: {
        header: null
      }
    },
    CallFieldExecutive: {
      screen: CallFieldExecutive,
      navigationOptions: {
        header: null
      }
    },
    ReceiveCall: {
      screen: ReceiveCall,
      navigationOptions: {
        header: null
      }
    },
    CallAlert: {
      screen: CallAlert,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: 'ListFieldExecutives'
  }
)

const RootNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthNavigation,
    App: AppNavigation,
    CustomerApp: CustomerAppNavigation
  },
  {
    initialRouteName: 'AuthLoading'
  }
)

const AppContainer = createAppContainer(RootNavigator)

export default AppContainer
