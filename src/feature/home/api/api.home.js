import Config from 'react-native-config'
import firebase from 'react-native-firebase'
import { eventChannel } from 'redux-saga'
import moment from 'moment'

export async function createCallSession(data, header) {
  console.log('the base ur is ' + Config.CORE_ENDPOINT_BASE_URL)

  const url = Config.CORE_ENDPOINT_BASE_URL + '/api/videosession/create'
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header,
      body: JSON.stringify({
        device_info: data.device_info,
        location: data.location
      })
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function createOneToOneCallSession(data, header) {
  console.log('the One to one url is ' + Config.CORE_ENDPOINT_BASE_URL)
  const url = Config.CORE_ENDPOINT_BASE_URL + '/api/videosession/create/to/' + data.toUser
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header,
      body: JSON.stringify({
        device_info: data.device_info,
        location: data.location
      })
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function disconnectSession(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + '/api/videosession/disconnect/' + data.id
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function disconnectOneToOneSession(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + '/api/videosession/onetoone/disconnect/' + data.id
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function changeDisplayMode(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL +
    '/api/videosession/displaymode/' +
    data.id +
    '/' +
    data.displayMode
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'PUT',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export function incomingMessageEventChannel(data) {
  let { sesion_id, member_entity } = data
  let channelMessagesRef = firebase
    .database()
    .ref(
      'ionassist-root/sessions_conversation/' + member_entity + '/' + sesion_id
    )
  const listener = eventChannel(emit => {
    channelMessagesRef.orderByChild('created_on').on('child_added', data => {
      console.log('child added' + data)
      emit(data.val())
    })
    return () => {
      return channelMessagesRef.off('child_added')
    }
  })
  return listener
}

export async function getLicenceValid(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + '/api/entity/valid/' + data.member_entity
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'GET',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function connectCall(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL +
    '/api/videosession/connect/' +
    data.firebaseid
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function connectOneToOneCall(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL +
    '/api/videosession/onetoone/connect/' +
    data.firebaseid
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function getAllEndUsers(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + '/api/entity/' + data.entityId + '/drivers'
  try {
    let response = await fetch(url, {
      method: 'GET',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function translateData(data, header) {
  const url =
    'https://translation.googleapis.com/language/translate/v2/?q=' +
    data.message.payload +
    '&source=' +
    data.from +
    '&target=' +
    data.to +
    '&key=' +
    Config.GOOGLE_API_MAP_KEY

  try {
    let response = await fetch(url, {
      method: 'GET',
      headers: header
    })

    console.log('+++++ ' + JSON.stringify(response))
    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function sendJobNumber(data, header) {
  console.log('the base ur is ' + Config.CORE_ENDPOINT_BASE_URL)

  const url = Config.CORE_ENDPOINT_BASE_URL + '/api/videosession/jobnumber/' + data.firebaseid
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header,
      body: JSON.stringify({
        jobnumber: data.jobnumber
      })
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function registerFCMToken(data, header) {
  const url = Config.CORE_ENDPOINT_BASE_URL + '/api/account/device'
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header,
      body: JSON.stringify({
        device_id: data.device_id,
        fcm_token: data.fcm_token
      })
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function ignoreCall(data, header) {
  const url = Config.CORE_ENDPOINT_BASE_URL + '/api/videosession/ignore/' + data.firebaseId
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header,
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function getPreSignedUrlRequest(data, header) {
  const url =
    Config.CORE_ENDPOINT_BASE_URL + '/api/videoSession/record/preSignedUrl?file_name=' + data.fileName + '&content_type=' + data.ContentType
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'GET',
      headers: header
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}

export async function sendRecordedVideoInfo(data, header) {
  const url = Config.CORE_ENDPOINT_BASE_URL + '/api/videosession/record'
  debugger;
  console.log(url)
  try {
    let response = await fetch(url, {
      method: 'POST',
      headers: header,
      body: JSON.stringify({
        file_name: data.file_name,
        path: data.path,
        record_start_time: data.record_start_time,
        record_end_time: data.record_end_time,
        latitude: data.latitude.toString(),
        longitude: data.longitude.toString()
      })
    })

    return {
      status: response.ok,
      data: await response.json()
    }
  } catch (e) {
    console.error(e)
    throw e
  }
}
