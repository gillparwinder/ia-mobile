import { DefaultTheme } from "react-native-paper";
import Config from "react-native-config";
export const theme = {
  ...DefaultTheme
};

export const APPID = Config.API_KEY_AGORA;

export const title = "IonAssist";
