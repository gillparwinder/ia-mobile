// import React, {  } from 'react'

import React, { useState, useEffect, Component } from 'react';
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
  BackHandler
} from 'react-native'
import {
  createCallSession,
  disconnectSession,
  changeCamera,
  setLocation,
  getLicenceValid,
  setCallInActive,
  setCallActive,
  registerFCMToken,
  saveCurrentCall
} from '../../actions/actions'
import { resetLogin } from '../../../login/actions/login'
import { logout, validateGuestUser } from '../../../login/actions/login'
import { connect } from 'react-redux'
import styles from './styles'
import { debounce } from 'lodash'
import { Icon } from 'react-native-elements'
import { checkPermissions, getGeoLocation, requsestLocationAccessIos } from '../../../../helpers/location'
import { requestPermissions } from '../../../../helpers/permissions'
import RNAndroidLocationEnabler from 'react-native-android-location-enabler'
import { getDeviceInformation } from '../../../../helpers/helper'
import { requestFCM } from '../../../../helpers/firebaseHelp'
import { NavigationEvents } from 'react-navigation'
import Header from '../../../components/header'
import Layout from '../../../components/layout';
import CallService from '../../../components/callService'
import IOSCallKeep from '../../../components/iosCallKeep'

const isIOS = Platform.OS === 'ios';

class Index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      location: false,
      loader: false
    }
  }

  componentWillMount() {
    requestPermissions();
    requsestLocationAccessIos();
    requestFCM((status, data) => {
      if (status == 'success') {
        this.props.registerFCMToken(data.deviceId, data.fcmToken);
      }
      else {
        console.log("Somthing went wrong in accessing fcmtoken");
      }
    });
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackPress
    );
    this.props.getLicenceValid(this.props.member_entity)
    if (!this.props.loggedIn) {
      this.props.navigation.navigate('Login')
    }
    if (this.props.role != 'CENTRAL_SUPPORT_EXECUTIVE') {
      if (Platform.OS == 'android') {
        CallService.stop();
        CallService.start(this.props.member_entity, this.props.role, this.props.user_name);
      }
    }

    this.enableLocation();
  }

  componentWillUpdate(nextProps) {
    if (nextProps.loggedIn != this.props.loggedIn && !this.props.loggedIn) {
      this.props.logout()
    }
  }

  componentDidUpdate(nextProps) {
    if (nextProps.loggedIn !== this.props.loggedIn) {
      if (!this.props.loggedIn) {
        this.props.navigation.navigate('Login')
      }
    }

    if (nextProps.licenceValid !== this.props.licenceValid) {
      if (!this.props.licenceValid) {
        this.props.resetLogin()
        this.props.navigation.navigate('Login')
      }
    }

    if (nextProps.sesionDetailsSuccess !== this.props.sesionDetailsSuccess) {
      if (this.props.sesionDetailsSuccess) {
        this.props.navigation.navigate('Agora')
      } else {
        this.props.setCallInActive();
        this.setState({ loader: false })
      }
    }
  }

  componentWillUnmount() {
  }

  handleBackPress = () => {
    BackHandler.exitApp();
  };

  enableLocation = async () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000
    })
      .then(data => {
        console.log('location enabled' + data)
        this.setState({ location: true })
      })
      .catch(err => {
        console.log(err)
      })
  }

  connectWithinfo = position => {
    let deviceInfo = getDeviceInformation()

    if (this.state.loader) {
      this.props.setCallActive();
      this.props.createCallSession(deviceInfo, position)
    }
  }

  connectSession = async () => {
    let permissionName = 'ACCESS_FINE_LOCATION'
    let permissionTitle = 'Device location permission'
    let permissionMessage = 'This form required device current location'

    if (Platform.OS == 'android') {
      if (Platform.Version >= 23 && this.state.location) {
        checkPermissions(permissionName, permissionTitle, permissionMessage)
          .then(response => {
            if (response.permission) {
              getGeoLocation({ highAccuracy: true }, (position, err) => {
                if (err) {
                  this.connectWithinfo('')
                } else {
                  this.props.setLocation(position.longitude, position.latitude)

                  if (!this.props.sessionRequest) this.connectWithinfo(position)
                }
              })
            } else {
              this.connectWithinfo('')
            }
          })
          .catch(response => console.log(response.err))
      } else {
        let location = await this.enableLocation()
        getGeoLocation({ highAccuracy: true }, (position, err) => {
          if (err) {
            this.connectWithinfo('')
          }
          this.props.setLocation(position.longitude, position.latitude)

          if (!this.props.sessionRequest) this.connectWithinfo(position)
          else {
            this.setState({ loader: false })
          }
        })
      }
    }
    else {
      requsestLocationAccessIos();
      navigator.geolocation.getCurrentPosition(
        position => {
          this.props.setLocation(position.coords.longitude, position.coords.latitude);
          if (!this.props.sessionRequest) this.connectWithinfo(position.coords)
        },
        error => {
          console.log(error);
          this.connectWithinfo('');
        },
        {
          enableHighAccuracy: false,
          timeout: 10000,
          maxAge: 0,
        }
      );
    }
  }

  render() {
    return (
      <Layout>
        <Header navigate={this.props.navigation.navigate} />
        <NavigationEvents
          onWillFocus={payload => {
            console.log('willfocus *******')

            if (this.props.role == 'GUEST_USER') {
              console.log('+++++++++++++validateGuestUser')
              this.props.validateGuestUser(this.props.member_entity)
            }
          }}
        />

        {isIOS ? <IOSCallKeep navigate={this.props.navigation.navigate} initiatedBy={'FieldSupport'} /> : null}


        <View style={styles.callContainer}>
          {/* <View>
            <Image
              source={require('../../../../assests/images/call.png')}
              resizeMode='contain'
              style={styles.callImag}
            />
          </View> */}
          {!this.state.loader ? (
            <View>
              {this.props.feature_config.allow_call ?
                <TouchableOpacity style={styles.callTextOuter} onPress={debounce(() => {
                  if (!this.props.sessionRequest) {
                    this.setState({ loader: true })
                    this.connectSession()
                  }
                }, 1000)} disabled={this.state.loader}  >
                  <Image
                    source={require('../../../../assests/images/iconcall.png')}
                    resizeMode='contain'
                    style={styles.callIconImag}
                  />
                  <Text style={{ marginLeft: 20, fontWeight: 'bold', fontSize: 16, color: '#242E42', textTransform: "uppercase" }}>
                    Call customer support
              </Text>
                </TouchableOpacity>
                : null}

              {this.props.feature_config.allow_record ?
                <TouchableOpacity style={[styles.callTextOuter, { marginTop: 30 }]} onPress={() => { this.props.navigation.navigate("VideoRecord") }} >
                  <Icon
                    name="perm-camera-mic"
                    type="material"
                    color="red"
                    size={30}
                  />
                  <Text style={{ marginLeft: 20, fontWeight: 'bold', fontSize: 16, color: '#242E42', textTransform: "uppercase" }}>
                    start Video Recording
                </Text>
                </TouchableOpacity>
                : null}
            </View>

          ) : (
              <View style={[styles.callTextOuter, { minWidth: 250, justifyContent: 'center', alignItems: 'center' }]}>
                <View style={{ flexDirection: 'row' }}>
                  <Text
                    style={{ fontWeight: '400', fontSize: 18, color: '#000' }}
                  >
                    Calling ...
                </Text>
                  <ActivityIndicator size='small' color='#0000ff' />
                </View>
              </View>
            )}
        </View>

        <View style={{ position: 'absolute', bottom: 10, width: '100%' }}>
          <View style={styles.inforow}>
            <Text style={styles.infoLabel}>Username : </Text>
            <Text style={styles.infoValue}>{this.props.user_name}</Text>
          </View>
          <View style={styles.inforow}>
            <Text style={styles.infoLabel}>Client : </Text>
            <Text style={styles.infoValue}>
              {this.props.entity_name}
            </Text>
          </View>
          {this.props.role == "GUEST_USER" ?
            <View style={{ alignItems: 'center' }}>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate("MemberEntity") }} style={styles.btnChangeClient} >
                <Text style={{ fontWeight: '400', fontSize: 14, color: '#000' }}>Change Client</Text>
              </TouchableOpacity>
            </View>
            : null}
        </View>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    count: state.homeReducer.count,
    sesionDetails: state.homeReducer.sesionDetails,
    sesionDetailsSuccess: state.homeReducer.sesionDetailsSuccess,
    loggedIn: state.loginReducer.loggedIn,
    role: state.loginReducer.role,
    user_name: state.loginReducer.user_name,
    sessionRequest: state.homeReducer.sessionRequest,
    camera: state.homeReducer.camera,
    location: state.homeReducer.location,
    licenceValid: state.homeReducer.licenceValid,
    member_entity: state.loginReducer.member_entity,
    member_entities: state.loginReducer.member_entities,
    entity_name: state.loginReducer.entity_name,
    licenceValidRequest: state.homeReducer.licenceValidRequest,
    feature_config: state.loginReducer.feature_config
  }
}

const mapDispatchToProps = {
  createCallSession,
  logout,
  disconnectSession,
  changeCamera,
  setLocation,
  getLicenceValid,
  resetLogin,
  validateGuestUser,
  setCallInActive,
  setCallActive,
  registerFCMToken,
  saveCurrentCall
}

export default connect(mapStateToProps, mapDispatchToProps)(Index)


