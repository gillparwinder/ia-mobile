import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  FlatList,
  YellowBox,
  TextInput,
  StyleSheet,
  Button,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  Modal,
  NativeEventEmitter,
  NativeModules,
  BackAndroid,
  BackHandler,
  Platform,
  KeyboardAvoidingView
} from "react-native";
import styles from "./styles";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";

import firebase from "react-native-firebase";
import Orientation from "react-native-orientation";
import Config from "react-native-config";
import { APPID } from "../../agoraSettings";
import { RtcEngine, AgoraView } from "react-native-agora";
import {
  connectOneToOneCall,
  disconnectOneToOneSession,
  resetAgora,
  changeCamera,
  changeDisplayMode,
  newMessage,
  translateFn,
  setCallInActive
} from "../../../actions/actions";
import moment from "moment";
import KeepAwake from "react-native-keep-awake";
import { captureScreen, releaseCapture } from "react-native-view-shot";
import RenderMessage from "../../../../components/renderMessage";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import ReactNativeInvokeApp from "../../../../components/invokeApp";
import RNCallKeep from 'react-native-callkeep';

const { Agora } = NativeModules;
const {
  FPS15,
  FPS30,
  FixedLandscape,
  FixedPortrait,
  Adaptative,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  CapturerOutputPreferenceAuto
} = Agora;

const firebaseConfig = {
  apiKey: Config.API_KEY,
  authDomain: Config.AUTH_DOMAIN,
  databaseURL: Config.DATABASE_URL,
  projectId: Config.PROJECT_ID,
  storageBucket: Config.STORAGE_BUCKET,
  messagingSenderId: Config.MESSAGING_SENDER_ID,
  appId: Config.APP_ID
};

let conversationRef = "";
let sessionRef = "";
YellowBox.ignoreWarnings(["Setting a timer"]);

class AttendCallFromSupport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orientation: "POTRAIT",
      showmessage: false,
      messageModal: false,
      peerIds: [],
      incomingCalls: [],
      joinSucceed: false,
      firebase_key: "",
      isAudioMuted: false,
      isVideoMuted: false,
      callEnding: false,
      callEndedBy: ""
    };
  }

  componentWillMount() {
    KeepAwake.activate();
    const config = {
      appid: APPID,
      channelProfile: 1,
      videoProfile: this.props.videoProfile,
      clientRole: Host,

      frameRate: FPS30,
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault,
      keepPrerotation: false,
      cameraIndex: 1025
    };
    console.log("[CONFIG]", JSON.stringify(config));
    RtcEngine.on("userJoined", data => {
      console.log("[RtcEngine] onUserJoined", data);
    });
    RtcEngine.on("userOffline", data => {
      console.log("[RtcEngine] onUserOffline", data);
      this.setState({
        joinSucceed: false
      });
    });
    RtcEngine.on("joinChannelSuccess", data => {
      console.log("[RtcEngine] onJoinChannelSuccess", data);
      // RtcEngine.startPreview().then(_ => {
      this.setState({
        joinSucceed: true,
        animating: false
      });
      // });
      this.props.connectOneToOneCall(
        this.props.navigation.getParam("firebaseKey", "")
      );
    });
    RtcEngine.init(config);
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackPress
    );

    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    Orientation.unlockAllOrientations();
    Orientation.addOrientationListener(this._orientationDidChange);

    RtcEngine.joinChannel(
      this.props.navigation.getParam("sessionId", ""),
      Math.floor(Math.random() * 100)
    );
    if (this.props.camera === "FRONT") {
      RtcEngine.switchCamera();
      this.props.changeCamera("BACK");
    }

    this.setState({
      firebase_key: this.props.navigation.getParam("firebaseKey", "")
    });

    sessionRef = firebase
      .database()
      .ref(
        "ionassist-root/one_to_one/" +
        this.props.member_entity + "/" + this.props.navigation.getParam("firebaseKey", "")
      );

    conversationRef = firebase
      .database()
      .ref(
        "ionassist-root/sessions_conversation/" +
        this.props.member_entity + "/" + this.props.navigation.getParam("sessionId", "")
      );

    sessionRef.on("child_changed", snapshot => {
      if (snapshot.val() === "DISCONNECTED") {
        this.disconnectIosCall();
        console.log(
          "child changed to discontected. observing from accept call page"
        );
        this.setState({ callEnding: true });
        this.props.setCallInActive();
        Orientation.lockToPortrait();
        if (this.state.callEndedBy != 'you')
          this.goBack();
      }
    });

    conversationRef.on("child_added", snapshot => {
      let incomingMessages = snapshot.val();
      if (
        this.props.language &&
        incomingMessages.language_code &&
        incomingMessages.language_code !== this.props.language
      ) {
        this.props.translateFn(
          incomingMessages.language_code,
          this.props.language,
          incomingMessages
        );
      } else this.props.newMessage(incomingMessages);
      this.setState({ showmessage: true, messageModal: true });
      Orientation.lockToPortrait();

      if (this.state.orientation === '"LANDSCAPE"') {
        this.setState({ orientation: "POTRAIT" });
        if (this.props.sesionDetails.id) {
          this.props.changeDisplayMode(this.props.sesionDetails.id, "POTRAIT");
        }
        Orientation.lockToPortrait();
      }
      this._scrollView
        ? this._scrollView.scrollToEnd({ animated: true })
        : null;
      this.secondTextInput ? this.secondTextInput.focus() : null;
    });
  }

  componentWillUnmount = () => {
    KeepAwake.deactivate();
    RtcEngine.destroy();
    RtcEngine.removeAllListeners();
    this.backHandler.remove();
  };

  handleBackPress = () => {
    this.disconnectSession();
  };

  _orientationDidChange = orientation => {
    if (orientation === "LANDSCAPE") {
      this.setState({ orientation: "LANDSCAPE" });
      if (this.props.navigation.getParam("firebaseKey", ""))
        this.props.changeDisplayMode(
          this.props.navigation.getParam("firebaseKey", ""),
          "LANDSCAPE"
        );
    } else {
      this.setState({ orientation: "POTRAIT" });

      if (this.props.navigation.getParam("firebaseKey", ""))
        this.props.changeDisplayMode(
          this.props.navigation.getParam("firebaseKey", ""),
          "POTRAIT"
        );
    }
  };

  disconnectIosCall = () => {
    if (Platform.OS == 'ios')
      RNCallKeep.endCall(this.props.currentCallInfo.callUUID);
  }


  disconnectSession = () => {
    this.disconnectIosCall();
    this.props.disconnectOneToOneSession(this.state.firebase_key);
    this.setState({ callEnding: true, callEndedBy: 'you' });
    this.props.setCallInActive();
    this.goBack();
  };

  goBack = () => {
    RtcEngine.leaveChannel()
      .then(_ => {
        this.props.navigation.navigate("Home");
      })
      .catch(err => {
        console.log("[agora]: err", err);
      });
  }

  showmessagefn = () => {
    Orientation.lockToPortrait();
    conversationRef.once("value", snapshot => {
      if (this.props.signals.length == 0 && snapshot.val()) {
        this.props.newMessage(snapshot.val());
      }
    });
    this.setState({ showmessage: true, messageModal: true });
  };

  setmessageVisible(visible) {
    this.setState({ messageModal: visible });
  }

  sendSignal = type => {
    let sendingObj = {
      message_type: type,
      payload: this.state.message,
      author: this.props.user_id,
      author_name: this.props.user_name,
      created_on: moment.utc().valueOf(),
      language_code: this.props.language
    };

    conversationRef
      .push(sendingObj)
      .then(data => {
        this.setState({ message: "" });
        this._scrollView.scrollToEnd({ animated: true });
      })
      .catch(error => {
        // error callback
        console.log("error ", error);
      });
  };

  toggleMuteVideo = () => {
    RtcEngine.muteLocalVideoStream(!this.state.isVideoMuted);
    this.setState({ isVideoMuted: !this.state.isVideoMuted });
  };

  toggleMuteAudio = () => {
    RtcEngine.muteLocalAudioStream(!this.state.isAudioMuted);
    this.setState({ isAudioMuted: !this.state.isAudioMuted });
  };

  renderCallInfo = () => {
    return (
      <View style={styles.bottomButtonOuter}>
        <View style={{ display: "flex", flexDirection: "row" }}>
          <TouchableOpacity onPress={() => this.toggleMuteAudio()}>
            <View
              style={[
                styles.btnMuteAudio,
                this.state.isAudioMuted ? styles.btnInActive : styles.btnActive
              ]}
            >
              {this.state.isAudioMuted ? (
                <Icon name="mic-off" type="material" color="#fff" size={20} />
              ) : (
                  <Icon name="mic-none" type="material" color="#fff" size={20} />
                )}
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.disconnectSession()}>
            <View style={styles.btnCallEnd}>
              <Icon name="call-end" type="material" color="#fff" size={20} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.toggleMuteVideo()}>
            <View
              style={[
                styles.btnMuteVideo,
                this.state.isVideoMuted ? styles.btnInActive : styles.btnActive
              ]}
            >
              {this.state.isVideoMuted ? (
                <Icon
                  name="videocam-off"
                  type="material"
                  color="#fff"
                  size={20}
                />
              ) : (
                  <Icon name="videocam" type="material" color="#fff" size={20} />
                )}
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={{ position: "absolute", right: 10 }}
          onPress={() => this.showmessagefn()}
        >
          <View style={styles.btnShowMessage}>
            <Icon name="message" type="material" color="#fff" size={20} />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  closeChat = () => {
    this.setState({ showmessage: false, messageModal: false });
    Orientation.unlockAllOrientations();
  };

  renderMessage = () => {
    const borderTopColormodal = !this.state.messageModal
      ? styles.borderGrey
      : styles.borderWhite;
    return (
      <View style={styles.messageModalout}>
        {this.state.messageModal ? (
          <View style={styles.messageModal}>
            <View style={styles.topLine} />
            <TouchableHighlight
              style={styles.closeMessage}
              onPress={() => this.setmessageVisible(false)}
            >
              <View style={styles.closeBotton}>
                <Icon
                  name="expand-more"
                  type="material"
                  color="#fff"
                  size={20}
                />
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.closechat}
              onPress={() => this.closeChat()}
            >
              <View style={styles.closeBotton}>
                <Icon name="close" type="material" color="#fff" size={20} />
              </View>
            </TouchableHighlight>
            <View
              style={{ backgroundColor: "#fff", height: 240, width: "100%" }}
            >
              <TouchableWithoutFeedback style={styles.messageModalcontainer}>
                <View style={styles.messagemodalOuter}>
                  <ScrollView
                    style={{ width: DEVICE_WIDTH, height: "100%" }}
                    ref={ref => {
                      this._scrollView = ref;
                    }}
                  >
                    {this.props.signals && this.props.signals.length > 0
                      ? this.props.signals.map((item, index) => {
                        return (
                          <RenderMessage
                            key={index}
                            index={index}
                            item={item}
                            user_id={this.props.user_id}
                          ></RenderMessage>
                        );
                      })
                      : null}
                  </ScrollView>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        ) : null}
        <View style={{ flex: 1 }}>
          <View style={styles.messageFooterContainer}>
            {!this.state.messageModal ? (
              <TouchableOpacity
                style={styles.expandeMessage}
                onPress={() => this.setmessageVisible(true)}
              >
                <View style={styles.expandeButton}>
                  <Icon
                    name="expand-less"
                    type="material"
                    color="#fff"
                    size={20}
                  />
                </View>
              </TouchableOpacity>
            ) : null}
            <View style={[styles.bubbleTop, borderTopColormodal]}>
              <View style={styles.sendBubble}>
                <TextInput
                  ref={input => {
                    this.secondTextInput = input;
                  }}
                  style={styles.inputOuter}
                  value={this.state.message}
                  onChangeText={message => this.setState({ message })}
                />
                <TouchableOpacity
                  style={styles.sendButton}
                  onPress={() => this.sendSignal("text")}
                >
                  <Icon
                    name="arrow-forward"
                    type="material"
                    color="#9e9e9e"
                    size={30}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == "ios" ? "padding" : "height"}>
      {
        this.state.joinSucceed ? (
          this.props.disableSessionOneToOneRequest || this.state.callEnding ? (
            <View
              style={{
                flex: 1,
                junullstifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text>Ending Call...</Text>
            </View>
          ) : (
              <View
                style={{
                  flex: 1,
                  backgroundColor: "#fff",
                  junullstifyContent: "center",
                  alignItems: "center"
                }}
              >
                <AgoraView
                  style={{ width: "100%", height: "100%" }}
                  showLocalVideo={true}
                />
                {this.renderCallInfo()}
                {this.state.showmessage ? this.renderMessage() : null}
              </View>
            )
        ) : (
            <View
              style={{ flex: 1, junullstifyContent: "center", alignItems: "center" }}
            >
              <Text>Connecting...</Text>
            </View>
          )
      }
    </KeyboardAvoidingView>
  }
}
const mapStateToProps = state => {
  return {
    count: state.homeReducer.count,
    sesionDetails: state.homeReducer.sesionDetails,
    loggedIn: state.loginReducer.loggedIn,
    language: state.loginReducer.language,
    user_name: state.loginReducer.user_name,
    user_id: state.loginReducer.user_id,
    member_entity: state.loginReducer.member_entity,
    sessionRequest: state.homeReducer.sessionRequest,
    camera: state.homeReducer.camera,
    signals: state.homeReducer.signals,
    disableSessionOneToOneRequest: state.homeReducer.disableSessionOneToOneRequest,
    currentCallInfo: state.homeReducer.currentCallInfo
  };
};

const mapDispatchToProps = {
  connectOneToOneCall,
  disconnectOneToOneSession,
  resetAgora,
  changeCamera,
  changeDisplayMode,
  newMessage,
  translateFn,
  setCallInActive
};

export default connect(mapStateToProps, mapDispatchToProps)(AttendCallFromSupport);
