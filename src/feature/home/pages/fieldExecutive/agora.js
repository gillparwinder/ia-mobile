import React, { Component } from "react";
import {
  TextInput,
  Text,
  View,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  ScrollView,
  Image,
  TouchableHighlight,
  TouchableWithoutFeedback,
  NativeModules,
  YellowBox,
  BackHandler,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import styles from "./styles";
import {
  createCallSession,
  disconnectSession,
  changeCamera,
  changeDisplayMode,
  resetAgora,
  newMessage,
  translateFn,
  setCallInActive
} from "../../actions/actions";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { Icon } from "react-native-elements";
import Config from "react-native-config";
import _ from "lodash";
import Orientation from "react-native-orientation";
import { RtcEngine, AgoraView } from "react-native-agora";
import { APPID } from "../agoraSettings";
import firebase from "react-native-firebase";
import moment from "moment";
import KeepAwake from "react-native-keep-awake";
import RenderMessage from "../../../components/renderMessage";

const { Agora } = NativeModules;

const {
  FPS15,
  FPS30,
  FixedLandscape,
  FixedPortrait,
  Adaptative,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  CapturerOutputPreferenceAuto
} = Agora;

const firebaseConfig = {
  apiKey: Config.API_KEY,
  authDomain: Config.AUTH_DOMAIN,
  databaseURL: Config.DATABASE_URL,
  projectId: Config.PROJECT_ID,
  storageBucket: Config.STORAGE_BUCKET,
  messagingSenderId: Config.MESSAGING_SENDER_ID,
  appId: Config.APP_ID
};
let conversationRef = "";
let sessionRef = "";
YellowBox.ignoreWarnings(["Setting a timer"]);
class AgoraComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      orientation: "POTRAIT",
      showmessage: false,
      messageModal: false,
      signals: [],
      messages: [],
      connected: false,
      timer: null,
      attended: false,
      isAudioMuted: false,
      isVideoMuted: false,
      imageURI: null,
      disconnecting: false
    };
  }

  componentWillMount() {
    KeepAwake.activate();
    const config = {
      appid: APPID,
      channelProfile: 1,
      videoProfile: this.props.videoProfile,
      clientRole: Host,
      frameRate: FPS30,
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault,
      keepPrerotation: false,
      cameraIndex: 1025
    };
    console.log("[CONFIG]", JSON.stringify(config));
    RtcEngine.on("firstRemoteVideoDecoded", data => {
      console.log("[RtcEngine] onFirstRemoteVideoDecoded", data);
    });
    RtcEngine.on("userJoined", data => {
      console.log("[RtcEngine] onUserJoined", data);
    });
    RtcEngine.on("userOffline", data => {
      console.log("[RtcEngine] onUserOffline", data);
      this.disconnectSession();
    });
    RtcEngine.on("joinChannelSuccess", data => {
      console.log("[RtcEngine] onJoinChannelSuccess", data);
      this.setState({
        joinSucceed: true,
        animating: false
      });
    });
    RtcEngine.on("clientRoleChanged", data => {
      console.log("[RtcEngine] onClientRoleChanged", data);
    });
    RtcEngine.on("error", data => {
      console.log("[RtcEngine] onError", data);
      if (data.error === 17) {
        this.props.disconnectSession(this.props.sesionDetails.id);
        Orientation.lockToPortrait();
        this.props.resetAgora();
        RtcEngine.leaveChannel().then(_ => {
          this.setState({
            joinSucceed: false
          });
          const { state, goBack } = this.props.navigation;
          goBack();
        });
      }
    });
    RtcEngine.init(config);
  }

  componentDidMount() {
    debugger;
    let timer = setInterval(this.unanswered, 100000);
    this.setState({ timer });
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackPress
    );
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }
    Orientation.unlockAllOrientations();
    Orientation.addOrientationListener(this._orientationDidChange);

    conversationRef = firebase
      .database()
      .ref(
        "ionassist-root/sessions_conversation/" +
        this.props.sesionDetails.member_entity +
        "/" +
        this.props.sesionDetails.sesion_id
      );

    sessionRef = firebase
      .database()
      .ref(
        "ionassist-root/sessions/" +
        this.props.sesionDetails.member_entity +
        "/" +
        this.props.sesionDetails.firebase_id
      );
    debugger;
    console.log(sessionRef);

    sessionRef.on("child_changed", snapshot => {
      debugger;
      if (snapshot.val() === "ACTIVE") {
        debugger;
        if (!this.state.connected) {
          clearInterval(this.state.timer);
          this.setState({
            connected: true
          });
          RtcEngine.joinChannel(
            this.props.sesionDetails.sesion_id,
            Math.floor(Math.random() * 100)
          );
          RtcEngine.enableAudioVolumeIndication(500, 3);
          if (this.props.camera === "FRONT") {
            RtcEngine.switchCamera();
            this.props.changeCamera("BACK");
          }
          RtcEngine.getSdkVersion(version => {
            console.log("[RtcEngine] getSdkVersion", version);
          });
        }
      } else if (snapshot.val() === "DISCONNECTED") {
        Orientation.lockToPortrait();
        this.props.setCallInActive();
        this.setState({ disconnecting: true });
        RtcEngine.leaveChannel()
          .then(_ => {
            this.props.resetAgora();
            this.props.navigation.navigate("Home");
          })
          .catch(err => {
            console.log("[agora]: err", err);
          });
      }
    });

    sessionRef.on("value", snapshot => {
      if (snapshot.val().status === "ACTIVE") {
        if (!this.state.connected) {
          clearInterval(this.state.timer);
          this.setState({
            connected: true
          });
          RtcEngine.joinChannel(
            this.props.sesionDetails.sesion_id,
            Math.floor(Math.random() * 100)
          );
          RtcEngine.enableAudioVolumeIndication(500, 3);
          if (this.props.camera === "FRONT") {
            RtcEngine.switchCamera();
            this.props.changeCamera("BACK");
          }
          RtcEngine.getSdkVersion(version => {
            console.log("[RtcEngine] getSdkVersion", version);
          });
        }
      }
    });

    conversationRef.on("child_added", snapshot => {
      let incomingMessages = snapshot.val();
      if (
        this.props.language &&
        incomingMessages.language_code &&
        incomingMessages.language_code !== this.props.language
      )
        this.props.translateFn(
          incomingMessages.language_code,
          this.props.language,
          incomingMessages
        );
      else this.props.newMessage(incomingMessages);
      this.setState({ showmessage: true, messageModal: true });
      Orientation.lockToPortrait();

      if (this.state.orientation === '"LANDSCAPE"') {
        this.setState({ orientation: "POTRAIT" });
        if (this.props.sesionDetails.id)
          this.props.changeDisplayMode(this.props.sesionDetails.id, "POTRAIT");
        Orientation.lockToPortrait();
      }
      this._scrollView
        ? this._scrollView.scrollToEnd({ animated: true })
        : null;
      this.secondTextInput ? this.secondTextInput.focus() : null;
    });
  }

  componentWillUnmount() {
    KeepAwake.deactivate();
    RtcEngine.destroy();
    RtcEngine.removeAllListeners();
    this.backHandler.remove();
    clearInterval(this.state.timer);
  }

  _orientationDidChange = orientation => {
    if (orientation === "LANDSCAPE") {
      this.setState({ orientation: "LANDSCAPE" });
      if (this.props.sesionDetails.id)
        this.props.changeDisplayMode(
          this.props.sesionDetails.firebase_id,
          "LANDSCAPE"
        );
    } else {
      this.setState({ orientation: "POTRAIT" });

      if (this.props.sesionDetails.id)
        this.props.changeDisplayMode(
          this.props.sesionDetails.firebase_id,
          "POTRAIT"
        );
    }
  };

  handleBackPress = () => {
    this.disconnectSession();
  };

  disconnectSession = () => {
    this.props.disconnectSession(this.props.sesionDetails.firebase_id);
    this.setState({ disconnecting: true });
    this.props.setCallInActive();
    Orientation.lockToPortrait();
    RtcEngine.leaveChannel()
      .then(_ => {
        this.props.resetAgora();
        //this.props.navigation.navigate("Home");
      })
      .catch(err => {
        console.log("[agora]: err", err);
      });
  };

  unanswered = () => {
    console.log("Call unaswered ***************************************");
    this.disconnectSession();
  };

  switchCamera = () => {
    RtcEngine.switchCamera();
  };

  showmessagefn = () => {
    Orientation.lockToPortrait();
    conversationRef.once("value", snapshot => {
      if (this.props.signals.length == 0 && snapshot.val())
        this.props.newMessage(snapshot.val());
    });
    this.setState({ showmessage: true, messageModal: true });
  };

  toggleMuteVideo = () => {
    RtcEngine.muteLocalVideoStream(!this.state.isVideoMuted);
    this.setState({ isVideoMuted: !this.state.isVideoMuted });
  };

  toggleMuteAudio = () => {
    RtcEngine.muteLocalAudioStream(!this.state.isAudioMuted);
    this.setState({ isAudioMuted: !this.state.isAudioMuted });
  };

  renderCallInfo = () => {
    return (
      <View style={styles.bottomButtonOuter}>
        <View style={{ display: "flex", flexDirection: "row" }}>
          <TouchableOpacity onPress={() => this.toggleMuteAudio()}>
            <View
              style={[
                styles.btnMuteAudio,
                this.state.isAudioMuted ? styles.btnInActive : styles.btnActive
              ]}
            >
              {this.state.isAudioMuted ? (
                <Icon name="mic-off" type="material" color="#fff" size={20} />
              ) : (
                  <Icon name="mic-none" type="material" color="#fff" size={20} />
                )}
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.disconnectSession()}>
            <View style={styles.btnCallEnd}>
              <Icon name="call-end" type="material" color="#fff" size={20} />
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.toggleMuteVideo()}>
            <View
              style={[
                styles.btnMuteVideo,
                this.state.isVideoMuted ? styles.btnInActive : styles.btnActive
              ]}
            >
              {this.state.isVideoMuted ? (
                <Icon
                  name="videocam-off"
                  type="material"
                  color="#fff"
                  size={20}
                />
              ) : (
                  <Icon name="videocam" type="material" color="#fff" size={20} />
                )}
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          style={{ position: "absolute", right: 10 }}
          onPress={() => this.showmessagefn()}
        >
          <View style={styles.btnShowMessage}>
            <Icon name="message" type="material" color="#fff" size={20} />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  setmessageVisible(visible) {
    this.setState({ messageModal: visible });
  }

  sendSignal = type => {
    let sendingObj = {
      message_type: type,
      payload: this.state.message,
      author: this.props.user_id,
      author_name: this.props.user_name,
      created_on: moment.utc().valueOf(),
      language_code: this.props.language
    };

    conversationRef
      .push(sendingObj)
      .then(data => {
        this.setState({
          message: ""
        });
        this._scrollView.scrollToEnd({ animated: true });
      })
      .catch(error => {
        console.log("error ", error);
      });
  };

  closeChat = () => {
    this.setState({ showmessage: false, messageModal: false });
    Orientation.unlockAllOrientations();
  };

  renderMessage = () => {
    const borderTopColormodal = !this.state.messageModal
      ? styles.borderGrey
      : styles.borderWhite;
    return (
      <View style={styles.messageModalout}>
        {this.state.messageModal ? (
          <View style={styles.messageModal}>
            <View style={styles.topLine} />
            <TouchableHighlight
              style={styles.closeMessage}
              onPress={() => this.setmessageVisible(false)}
            >
              <View style={styles.closeBotton}>
                <Icon
                  name="expand-more"
                  type="material"
                  color="#fff"
                  size={20}
                />
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.closechat}
              onPress={() => this.closeChat()}
            >
              <View style={styles.closeBotton}>
                <Icon name="close" type="material" color="#fff" size={20} />
              </View>
            </TouchableHighlight>
            <View
              style={{ backgroundColor: "#fff", height: 240, width: "100%" }}
            >
              <TouchableWithoutFeedback style={styles.messageModalcontainer}>
                <View style={styles.messagemodalOuter}>
                  <ScrollView
                    style={{ width: DEVICE_WIDTH, height: "100%" }}
                    ref={ref => { this._scrollView = ref; }} >
                    {this.props.signals && this.props.signals.length > 0
                      ? this.props.signals.map((item, index) => {
                        return (
                          <RenderMessage key={index} index={index} item={item} user_id={this.props.user_id} ></RenderMessage>
                        );
                      })
                      : null}
                  </ScrollView>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        ) : null}
        <View style={{ flex: 1 }}>
          <View style={styles.messageFooterContainer}>
            {!this.state.messageModal ? (
              <TouchableOpacity
                style={styles.expandeMessage}
                onPress={() => this.setmessageVisible(true)}
              >
                <View style={styles.expandeButton}>
                  <Icon name="expand-less" type="material" color="#fff" size={20} />
                </View>
              </TouchableOpacity>
            ) : null}
            <View style={[styles.bubbleTop, borderTopColormodal]}>
              <View style={styles.sendBubble}>
                <TextInput
                  ref={input => {
                    this.secondTextInput = input;
                  }}
                  style={styles.inputOuter}
                  value={this.state.message}
                  onChangeText={message => this.setState({ message })}
                />
                <TouchableOpacity
                  style={styles.sendButton}
                  onPress={() => this.sendSignal("text")}
                >
                  <Icon
                    name="arrow-forward"
                    type="material"
                    color="#9e9e9e"
                    size={30}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  };

  render() {
    return <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == "ios" ? "padding" : "height"}>
      {!this.state.connected ? (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <ActivityIndicator size="large" color="#0000ff" />
          <Text>Waiting for Support executive to attend.</Text>
          {this.state.showmessage ? this.renderMessage() : this.renderCallInfo()}
        </View>
      ) : this.props.disableSessionRequest || this.state.disconnecting ? (
        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
          <ActivityIndicator size="large" color="#0000ff" />
          <Text>Ending Call...</Text>
          {this.state.showmessage ? this.renderMessage() : this.renderCallInfo()}
        </View>
      ) : (
            <View style={{ flex: 1 }}>
              <View style={styles.driverViewContainer}>
                <AgoraView
                  style={{ width: "100%", height: "100%" }}
                  showLocalVideo={true}
                />
                {this.state.showmessage
                  ? this.renderMessage()
                  : this.renderCallInfo()}
              </View>
              {this.state.showmessage ? (
                <View
                  style={{
                    position: "absolute",
                    top: 10,
                    left: DEVICE_WIDTH / 2 - 30
                  }}
                >
                  <TouchableOpacity onPress={() => this.disconnectSession()}>
                    <Image
                      source={require("../../../../assests/images/call-icon.png")}
                      resizeMode="contain"
                      style={{ width: 50, height: 50, margin: 10 }}
                      opacity={1}
                    />
                  </TouchableOpacity>
                </View>
              ) : null}
            </View>
          )}
    </KeyboardAvoidingView>
  }
}

const mapStateToProps = state => {
  return {
    count: state.homeReducer.count,
    sesionDetails: state.homeReducer.sesionDetails,
    disableSessionRequest: state.homeReducer.disableSessionRequest,
    sesionDetailsSuccess: state.homeReducer.sesionDetailsSuccess,
    loggedIn: state.loginReducer.loggedIn,
    language: state.loginReducer.language,
    user_name: state.loginReducer.user_name,
    user_id: state.loginReducer.user_id,
    sessionRequest: state.homeReducer.sessionRequest,
    camera: state.homeReducer.camera,
    signals: state.homeReducer.signals
  };
};

const mapDispatchToProps = {
  createCallSession,
  disconnectSession,
  changeCamera,
  changeDisplayMode,
  resetAgora,
  newMessage,
  translateFn,
  setCallInActive
};

export default connect(mapStateToProps, mapDispatchToProps)(AgoraComponent);
