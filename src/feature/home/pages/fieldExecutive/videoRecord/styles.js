import { StyleSheet, Dimensions } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        position: 'relative'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        height: 50,
        justifyContent: 'center',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#79BD4B',
        shadowColor: '#79BD4B',
        shadowOffset: {
            width: 4,
            height: 6
        },
        shadowRadius: 2.81,
        elevation: 8,
        marginRight: 10,
        marginLeft: 10,
        minWidth: 130
    },
    btnText: {
        fontWeight: 'bold',
        fontSize: 16,
        color: '#242E42',
        textTransform: "uppercase"
    },
    btnWrap: {
        position: 'absolute',
        width: '100%',
        bottom: 0,
        flexDirection: "row",
        // backgroundColor: 'rgba(8, 149, 230, 0.5)',
        justifyContent: "center",
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 10
    },
    backgroundVideo: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    }
})

export default styles