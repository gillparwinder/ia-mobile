import React, { Component } from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    Platform,
    ActivityIndicator,
    Alert
} from 'react-native'
import styles from './styles'
import Layout from '../../../../components/layout';
import { connect } from 'react-redux'
import { getPreSignedUrlRequest, sendRecordedVideoInfo } from '../../../actions/actions'
import { checkPermissions, getGeoLocation, requsestLocationAccessIos } from '../../../../../helpers/location'
import RNFS from 'react-native-fs'
import RNAndroidLocationEnabler from 'react-native-android-location-enabler'
import { Icon } from 'react-native-elements'

// import Video from 'react-native-video';
import { RNCamera } from 'react-native-camera';

const isIOS = Platform.OS === 'ios';
class VideoRecord extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recording: false,
            recordingStopped: false,
            processing: false,
            showVideo: false,
            videoUrl: '',
            timer: 180,
            latitude: '',
            longitude: '',
            location: false,
            recordStartTime: '',
            recordEndTime: ''
        }
    }
    componentWillMount() { }
    componentDidMount() {
        this.requestLocation();
    }
    componentWillUnmount() {
        clearInterval(this.interval);
    }

    startTimer() {
        this.interval = setInterval(
            () => {
                this.setState((prevState) => ({ timer: prevState.timer - 1 }))
                if (this.state.timer == 0) {
                    clearInterval(this.interval);
                }
            },
            1000
        );
    }


    componentDidUpdate(nextProps) {
        if (nextProps.preSignedRequestUrlSuccess != this.props.preSignedRequestUrlSuccess && this.props.preSignedRequestUrlSuccess) {
            this.uploadVideo(this.sendRecordVideoInfo);
        }
    }

    async uploadVideo(callback) {
        const xhr = new XMLHttpRequest()
        xhr.open('PUT', this.props.preSignedUrl.url)
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    callback("success");
                    console.log('video successfully uploaded to S3')
                } else {
                    callback("error");
                    console.log('Error while sending the image to S3')
                }
            }
        }
        xhr.setRequestHeader('Content-Type', 'video/mp4')
        let uri = this.state.videoUrl;
        if (isIOS)
            uri = this.state.videoUrl.replace("file://", "");

        xhr.send({ uri: uri, type: 'video/mp4', name: this.props.preSignedUrl.file_name });
    }

    sendRecordVideoInfo = (status) => {
        if (status == "success") {
            this.setState({ processing: false, recordingStopped: false });
            this.props.sendRecordedVideoInfo(this.props.preSignedUrl.file_name,
                this.props.preSignedUrl.path,
                this.state.recordStartTime,
                this.state.recordEndTime,
                this.state.latitude,
                this.state.longitude
            )

            Alert.alert("", "Uploaded successfully.");
            this.props.navigation.navigate("Home");
        }
        if (status == "error") {
            Alert.alert("", "Something went wrong. Please try again")
            this.props.navigation.navigate("Home");
            this.setState({ processing: false });
        }
    }

    startRecording = () => {
        this.setState({ recording: true, recordingStopped: false });
        this.startTimer();
        this.setState({ recordStartTime: new Date().toISOString() });

        this.camera
            .recordAsync(
                { videoBitrate: 1024 * 1024, maxDuration: 180, quality: "480p", mirrorVideo: true, }
            )
            .then((data) => { this.saveRecording(data) })
            .catch((err) => console.log('err', err));


        // default to mp4 for android as codec is not set
        // const { uri, codec = "mp4", isRecordingInterrupted = false } = await this.camera.recordAsync({ videoBitrate: 1024 * 1024, maxDuration: 180, quality: "480p" });
        // alert("recording stopped");

        // callback(this);
    }

    saveRecording(data) {
        this.setState({ recordEndTime: new Date().toISOString() });
        this.setState({ videoUrl: data.uri, processing: true, recording: false, recordingStopped: true });
        this.requestUpload();
    }

    enableLocation = async () => {
        RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({ interval: 10000, fastInterval: 5000 })
            .then(data => {
                console.log('location enabled' + data)
                this.setState({ location: true })
            })
            .catch(err => {
                console.log(err)
            })
    }

    requestLocation = () => {
        if (Platform.OS == 'ios')
            requsestLocationAccessIos();

        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({ latitude: position.coords.latitude, longitude: position.coords.longitude });
            },
            error => {
                console.log(error);
            },
            {
                enableHighAccuracy: false,
                timeout: 10000,
                maxAge: 0,
            }
        );
    }


    // startReRecording() {
    //     this.setState({ recording: true, recordingStopped: false });
    //     this.setState({ timer: 180 });
    //     clearInterval(this.interval);
    //     setTimeout(() => {
    //         this.startRecording(this.saveRecording);
    //     }, 1000)
    // }

    stopRecording = () => {
        this.camera.stopRecording();
    }

    onRecordingEnd = () => {
        // this.setState({ recording: false, recordingStopped: true });
    }

    // onRecordingStart = () => {

    // }

    // initiateRecording()

    // onStatusChange = (status) => {
    //     alert(status.cameraStatus);
    //     if (status.cameraStatus == 'READY') {
    //         this.startRecording();
    //     }
    //     else if (status.recordAudioPermissionStatus == 'AUTHORIZED') {
    //         alert("Camera permission not granted.")
    //     }
    // }

    // playVideo() {
    //     this.setState({ showVideo: true });
    // }

    requestUpload = () => {
        let dt = new Date();
        let fileName = dt.getDate() + '-' + dt.getMonth() + '-' + dt.getFullYear() + '-' + dt.getHours() + '-' + dt.getMinutes();
        this.props.getPreSignedUrlRequest('iaVideo' + fileName, 'video/mp4');
    }

    render() {
        const { recording, processing, recordingStopped } = this.state;

        let button = (
            <View>
            </View>
        );

        if (recording) {
            button = (
                <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center', alignItems: 'center', position: 'relative' }}>
                    <TouchableOpacity onPress={() => { this.stopRecording() }}
                        style={{ borderRadius: 30, borderColor: '#fff', borderWidth: 1, justifyContent: 'center', alignItems: 'center', width: 60, height: 60 }} >
                        <Icon name="stop" type="material" color="red" size={45} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}
                        style={{ position: 'absolute', right: 20 }}>
                        <Icon
                            name="cancel"
                            type="material"
                            color="#fff"
                            size={45}
                        />
                    </TouchableOpacity>
                </View>
            )
        }

        if (processing) {
            button = (
                <View style={styles.capture}>
                    <ActivityIndicator style={{ marginRight: 15 }} size='small' color='#4183f5' />
                    <Text style={styles.btnText}>Uploading....</Text>
                </View>
            );
        }

        return (
            <Layout>
                <View style={styles.container}>
                    <View style={{ position: 'absolute', backgroundColor: 'rgba(255,255,255,0.6)', zIndex: 100, width: '100%', top: 10, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                        {this.state.recording ?
                            <View style={{ flexDirection: 'row', padding: 5 }}>
                                <Text style={{ color: 'blue', fontWeight: 'bold' }}>Time remaining: </Text>
                                <Text style={{ color: 'red', fontWeight: 'bold' }}> {this.state.timer} seconds </Text>
                            </View>
                            : null}

                        {!this.state.recording && !this.state.recordingStopped ?
                            <Text style={{ color: 'blue', fontWeight: 'bold', padding: 5 }}>Max duration: 3 mins </Text>
                            : null}
                    </View>

                    <RNCamera
                        ref={ref => {
                            this.camera = ref;
                        }}
                        style={styles.preview}
                        type={RNCamera.Constants.Type.back}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        onRecordingEnd={this.onRecordingEnd}
                        onCameraReady={this.startRecording}
                        // onRecordingStart={this.onRecordingStart}
                        // onStatusChange={this.onStatusChange}
                        permissionDialogTitle={"Permission to use camera"}
                        permissionDialogMessage={
                            "We need your permission to use your camera phone"
                        }
                    />

                    {
                        this.state.recordingStopped ?
                            <View style={[{ flex: 1, position: 'absolute', backgroundColor: 'rgba(0,0,0,0.8)', width: '100%', height: '100%', justifyContent: 'center', alignItems: "center" }]}>
                                <View style={{ borderRadius: 5, borderWidth: 1, borderColor: '#fff', padding: 10, paddingRight: 30, paddingLeft: 30, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                    <ActivityIndicator style={{ marginRight: 15 }} size='small' color='#4183f5' />
                                    <Text style={{ fontWeight: 'bold', color: '#fff' }}>Uploading....</Text>
                                </View>
                            </View>
                            :
                            <View style={styles.btnWrap}>
                                {button}
                            </View>
                    }

                </View>
            </Layout>
        );
    }
}

const mapStateToProps = state => {
    return {
        processingRequest: state.homeReducer.processingRequest,
        preSignedUrl: state.homeReducer.preSignedUrl,
        preSignedRequestUrlSuccess: state.homeReducer.preSignedRequestUrlSuccess,
        user_name: state.loginReducer.user_name,
    }
}

const mapDispatchToProps = {
    getPreSignedUrlRequest,
    sendRecordedVideoInfo
}

export default connect(mapStateToProps, mapDispatchToProps)(VideoRecord)
