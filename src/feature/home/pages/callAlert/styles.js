import { StyleSheet, Dimensions } from 'react-native'
const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 15,
        paddingRight: 15,
        justifyContent: 'space-around',
        alignItems: "center"
    },
    nameHolder: {
        alignItems: 'center'
    },
    personWrap: {
        marginTop: 50,
        marginBottom: 10,
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: '#fff',
        borderWidth: 2,
        borderRadius: 50
    },
    txtIncomingCall: {
        color: '#fff',
        fontSize: 16,
        fontWeight: 'bold'
    },
    txtName: {
        color: '#fff',
        fontSize: 18,
        fontWeight: 'bold'
    },
    buttonHolder: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnCallEnd: {
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        width: 50,
        height: 50,
        marginRight: 30
    },
    btnCallAccept: {
        backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 50,
        width: 50,
        height: 50,
        marginLeft: 30
    }
})

export default styles
