import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    FlatList,
    NativeModules,
    YellowBox,
    BackHandler,
    NativeEventEmitter
} from "react-native";
import styles from "./styles";
import Layout from "../../../components/layout";
import firebase from "react-native-firebase";
import { connect } from "react-redux";
import Config from "react-native-config";
import { Icon } from 'react-native-elements'
import { disconnectSession, disconnectOneToOneSession, setCallActive, setCallInActive, ignoreCall } from '../../actions/actions'
import KeepAwake from 'react-native-keep-awake'
import SoundPlayer from 'react-native-sound-player'
import ReactNativeInvokeApp from '../../../components/invokeApp'
const { Agora } = NativeModules;
const {
    FPS15,
    FPS30,
    FixedLandscape,
    FixedPortrait,
    Adaptative,
    AudioProfileDefault,
    AudioScenarioDefault,
    Host,
    CapturerOutputPreferenceAuto
} = Agora;

const firebaseConfig = {
    apiKey: Config.API_KEY,
    authDomain: Config.AUTH_DOMAIN,
    databaseURL: Config.DATABASE_URL,
    projectId: Config.PROJECT_ID,
    storageBucket: Config.STORAGE_BUCKET,
    messagingSenderId: Config.MESSAGING_SENDER_ID,
    appId: Config.APP_ID
};

let sessionRef = "";
YellowBox.ignoreWarnings(["Setting a timer"]);

class CallAlert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sessionId: '',
            firebaseKey: '',
            requestedUser: '',
            userId: '',
            from: '',
            acceptCallRoute: '',
            rejectCallRoute: '',
            firebaseUrl: '',
            callAttended: false
        };
    }
    _onFinishedPlayingSubscription = null;

    componentWillMount() {
        this.setState({
            sessionId: this.props.navigation.getParam('sessionId', ''),
            firebaseKey: this.props.navigation.getParam('firebaseKey', ''),
            requestedUser: this.props.navigation.getParam('requestedUser', ''),
            userId: this.props.navigation.getParam('userId', ''),
            from: this.props.navigation.getParam('from', '')
        });

        if (this.props.navigation.getParam('from', '') == "fieldExecutive")
            this.setState({
                acceptCallRoute: "ReceiveCall", rejectCallRoute: "ListFieldExecutives", firebaseUrl: "ionassist-root/sessions/" + this.props.member_entity
            })
        else
            this.setState({
                acceptCallRoute: "AttendCallFromSupport", rejectCallRoute: "Home", firebaseUrl: "ionassist-root/one_to_one/" + this.props.member_entity
            })
    }

    componentDidMount() {
        if (!this.props.loggedIn) {
            this.props.navigation.navigate('Login');
        }
        else {
            this.backHandler = BackHandler.addEventListener(
                "hardwareBackPress",
                this.handleBackPress
            );
            KeepAwake.activate();
            this.playRingTone();
            _onFinishedPlayingSubscription = SoundPlayer.addEventListener('FinishedPlaying', ({ success }) => {
                console.log('finished playing', success);
                this.playRingTone();
            });

            if (!firebase.apps.length) {
                firebase.initializeApp(firebaseConfig);
            }

            sessionRef = firebase
                .database()
                .ref(this.state.firebaseUrl);

            sessionRef.on("child_changed", snapshot => {
                if ((snapshot.val().status == "DISCONNECTED" && snapshot.val().firebase_key == this.state.firebaseKey) ||
                    (snapshot.val().status == "ACTIVE" && snapshot.val().firebase_key == this.state.firebaseKey && snapshot.val().accepted_user_name != this.props.user_name)) {
                    if (!this.state.callAttended) {
                        this.props.setCallInActive();
                        this.callAttendedOrDisconnected();
                    }
                }
            });
        }
    }

    componentWillUnmount() {
        KeepAwake.deactivate();
        this.backHandler.remove();
        SoundPlayer.stop();
        _onFinishedPlayingSubscription.remove();
        console.log("callAlert unmounted");
    }

    handleBackPress = () => {
        this.disconnectCall();
    };

    callAttendedOrDisconnected = () => {
        this.clearSubscription();
        ReactNativeInvokeApp.detectLock();
        const eventEmitter = new NativeEventEmitter(NativeModules.ReactNativeInvokeApp);
        eventEmitter.addListener('phoneLocked', (data) => {
            data.isLocked == "locked" ? BackHandler.exitApp() : null;
            this.props.navigation.navigate(this.state.rejectCallRoute);
        });
    }

    acceptCall = () => {
        this.setState({ callAttended: true })
        this.clearSubscription();
        this.props.setCallActive();
        this.props.navigation.navigate(this.state.acceptCallRoute, { sessionId: this.state.sessionId, firebaseKey: this.state.firebaseKey });
    }

    disconnectCall = () => {
        this.props.setCallInActive();
        // this.state.from == "fieldExecutive" ? this.props.disconnectSession(this.state.firebaseKey) : this.props.disconnectOneToOneSession(this.state.firebaseKey);
        this.state.from == "fieldExecutive" ? this.props.ignoreCall(this.state.firebaseKey) : this.props.disconnectOneToOneSession(this.state.firebaseKey);

        this.callAttendedOrDisconnected();
    }

    clearSubscription = () => {
        KeepAwake.deactivate();
        SoundPlayer.stop();
        _onFinishedPlayingSubscription.remove();
    }

    playRingTone = () => {
        try {
            SoundPlayer.playSoundFile('ringtone', 'mp3');
            SoundPlayer.setVolume(100);
        } catch (e) {
            console.log(`cannot play the sound file`, e)
        }
    }

    // async getInfo() { // You need the keyword `async`
    //     try {
    //         const info = await SoundPlayer.getInfo() // Also, you need to await this because it is async
    //         console.log('getInfo', info) // {duration: 12.416, currentTime: 7.691}
    //     } catch (e) {
    //         console.log('There is no song playing', e)
    //     }
    // }

    render() {
        return (
            <Layout>
                <View style={styles.container}>
                    <View style={styles.nameHolder}>
                        <Text style={styles.txtIncomingCall}>Incoming Call</Text>
                        <View style={styles.personWrap}>
                            <Icon name='person' type='material' color='#fff' size={60} />
                        </View>
                        <Text style={styles.txtName}>{this.state.requestedUser}</Text>
                    </View>
                    <View style={styles.buttonHolder}>
                        <TouchableOpacity onPress={() => { this.disconnectCall() }}>
                            <View style={styles.btnCallEnd}>
                                <Icon name='call-end' type='material' color='#fff' size={20} />
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { this.acceptCall() }}>
                            <View style={styles.btnCallAccept}>
                                <Icon name='call' type='material' color='#fff' size={20} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </Layout>
        )
    }
}

const mapStateToProps = state => {
    return {
        count: state.homeReducer.count,
        sesionDetails: state.homeReducer.sesionDetails,
        sesionDetailsSuccess: state.homeReducer.sesionDetailsSuccess,
        language: state.loginReducer.language,
        user_name: state.loginReducer.user_name,
        user_id: state.loginReducer.user_id,
        member_entity: state.loginReducer.member_entity,
        sessionRequest: state.homeReducer.sessionRequest,
        loggedIn: state.loginReducer.loggedIn,
        camera: state.homeReducer.camera,
        signals: state.homeReducer.signals
    };
};

const mapDispatchToProps = {
    disconnectSession,
    disconnectOneToOneSession,
    setCallActive,
    setCallInActive,
    ignoreCall
};

export default connect(mapStateToProps, mapDispatchToProps)(CallAlert);
