import React, { Component } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  NativeModules,
  YellowBox,
  PermissionsAndroid,
  Platform,
  ActivityIndicator,
  Dimensions,
  DeviceEventEmitter,
  NativeEventEmitter,
  BackHandler,
  Modal
} from 'react-native'
import styles from './styles'
import Layout from '../../../../components/layout'
import Header from '../../../../components/header'
import firebase from 'react-native-firebase'
import { connect } from 'react-redux'
import {
  getAllEndUsers, setLocation, createOneToOneCallSession,
  setCallInActive, setCallActive, registerFCMToken
} from '../../../actions/actions'
import { Icon } from 'react-native-elements'
import { debounce } from 'lodash'
import {
  checkPermissions,
  getGeoLocation,
  requsestLocationAccessIos
} from '../../../../../helpers/location'
import RNAndroidLocationEnabler from 'react-native-android-location-enabler'
import { getDeviceInformation } from '../../../../../helpers/helper'
const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height
import CallService from '../../../../components/callService';
import Config from "react-native-config";
import { requestFCM } from '../../../../../helpers/firebaseHelp'
import { requestPermissions } from '../../../../../helpers/permissions'
import IOSCallKeep from '../../../../components/iosCallKeep'

const isIOS = Platform.OS === 'ios';

class ListFieldExecutives extends Component {
  constructor(props) {
    super(props)
    this.state = {
      location: false,
      loader: false,
      toUser: '',
      showFilter: false,
      isAscending: true,
      endUsers: [],
      refreshing: false
    }
  }

  handleBackPress = () => {
    BackHandler.exitApp();
  };

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      this.handleBackPress
    );
    if (this.props.role == 'CENTRAL_SUPPORT_EXECUTIVE') {
      console.log("Starting service from list end user page...");
      if (Platform.OS == 'android') {
        CallService.stop();
        CallService.start(this.props.member_entity, this.props.role, this.props.user_name);
      }
    }
  }

  componentWillMount() {
    this.props.getAllEndUsers(this.props.member_entity);
    requestPermissions();
    requsestLocationAccessIos();
    requestFCM((status, data) => {
      if (status == 'success') {
        this.props.registerFCMToken(data.deviceId, data.fcmToken);
      }
      else {
        console.log("Somthing went wrong in accessing fcmtoken");
      }
    });
  }

  componentDidUpdate(nextProps) {
    if (nextProps.getEndUsersSuccess !== this.props.getEndUsersSuccess && this.props.getEndUsersSuccess) {
      this.setAscending();
    }

    if (nextProps.sesionOneToOneDetailsSuccess !== this.props.sesionOneToOneDetailsSuccess) {
      if (this.props.sesionOneToOneDetailsSuccess) {
        this.setState({ loader: false, toUser: '' });
        this.props.navigation.navigate('CallFieldExecutive')
      } else {
        this.props.setCallInActive();
        this.setState({ loader: false })
      }
    }
  }

  setModalVisible(visible) {
    this.setState({ showFilter: visible });
  }

  enableLocation = async () => {
    RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
      interval: 10000,
      fastInterval: 5000
    })
      .then(data => {
        console.log('location enabled' + data)
        this.setState({ location: true })
      })
      .catch(err => {
        console.log(err)
      })
  }

  connectWithinfo = position => {
    let deviceInfo = getDeviceInformation()

    if (this.state.loader) {
      // this.setState({ loader: false });
      this.props.setCallActive();
      this.props.createOneToOneCallSession(
        deviceInfo,
        position,
        this.state.toUser
      )
    }
  }

  initiateCall = async toUser => {
    let permissionName = 'ACCESS_FINE_LOCATION'
    let permissionTitle = 'Device location permission'
    let permissionMessage = 'This form required device current location'
    this.setState({ toUser: toUser })
    if (Platform.OS == 'android') {
      if (Platform.Version >= 23 && this.state.location) {
        checkPermissions(permissionName, permissionTitle, permissionMessage)
          .then(response => {
            if (response.permission) {
              getGeoLocation({ highAccuracy: true }, (position, err) => {
                if (err) {
                  this.connectWithinfo('')
                } else {
                  this.props.setLocation(position.longitude, position.latitude)

                  if (!this.props.sessionRequest) this.connectWithinfo(position)
                }
              })
            } else {
              this.connectWithinfo('')
            }
          })
          .catch(response => console.log(response.err))
      } else {
        let location = await this.enableLocation()
        getGeoLocation({ highAccuracy: true }, (position, err) => {
          if (err) {
            this.connectWithinfo('')
          }
          this.props.setLocation(position.longitude, position.latitude)

          if (!this.props.sessionRequest) this.connectWithinfo(position)
          else {
            this.setState({ loader: false })
          }
        })
      }
    }
    else {
      requsestLocationAccessIos();
      navigator.geolocation.getCurrentPosition(
        position => {
          this.props.setLocation(position.coords.longitude, position.coords.latitude);
          if (!this.props.sessionRequest) this.connectWithinfo(position.coords)
        },
        error => {
          console.log(error);
          this.connectWithinfo('');
        },
        {
          enableHighAccuracy: false,
          timeout: 10000,
          maxAge: 0,
        }
      );
    }
  }

  getTile = (item) => {
    return (
      <View style={styles.tileContainer} key={Math.random().toString()}>
        {loader && toUser == item.user_name ?
          <View>
            <Text style={{ fontWeight: 'bold', color: '#00ff3a', fontSize: 18 }}>Calling {item.firstname} {item.lastname}...</Text>
          </View>
          : <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Icon name='person' type='material' color='#fff' size={30} />
            <View style={{ marginLeft: 10 }}>
              <Text style={styles.txtName}>
                {item.firstname} {item.lastname}
              </Text>
              <Text style={styles.txtUserName}>{item.user_name}</Text>
            </View>
          </View>}

        <TouchableOpacity
          onPress={debounce(() => {
            if (!this.props.sessionRequest) {
              this.setState({ loader: true })
              this.initiateCall(item.user_name)
            }
          }, 1000)}
          disabled={this.state.loader}
        >
          {loader && toUser == item.user_name ?
            <Icon name='call' type='material' color='#00ff3a' size={30} /> :
            <Icon name='call' type='material' color='#fff' size={30} />
          }
        </TouchableOpacity>
      </View>
    )
  }

  setAscending = () => {
    let users = this.props.endUsers;
    users.sort(function (a, b) {
      var nameA = a.firstname.toLowerCase(), nameB = b.firstname.toLowerCase()
      if (nameA < nameB) //sort string ascending
        return -1
      if (nameA > nameB)
        return 1
      return 0 //default return value (no sorting)
    });
    this.setState({ endUsers: users });
  }

  toggleSort = () => {
    this.setState({ refreshing: true }, () => {
      this.setState({ isAscending: !this.state.isAscending, endUsers: this.state.endUsers.reverse(), refreshing: false, showFilter: false });
    })
  }

  render() {
    return (
      <Layout>
        <Header navigate={this.props.navigation.navigate} />
        {isIOS ? <IOSCallKeep navigate={this.props.navigation.navigate} initiatedBy={'CentralSupport'} /> : null}
        <View style={styles.container}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginBottom: 5 }}>
            <TouchableOpacity onPress={() => { this.setModalVisible(true) }}>
              <Text style={{ color: '#fff', marginBottom: 10, fontWeight: 'bold', fontSize: 16 }}>Filter</Text>
            </TouchableOpacity></View>
          <FlatList
            data={this.state.endUsers}
            renderItem={({ item }) => this.getTile(item)}
            keyExtractor={(item, index) => index.toString()}
            extraData={loader = this.state.loader, toUser = this.state.toUser}
            // onRefresh={() => this.toggleSort()}
            refreshing={this.state.refreshing}
          />
          <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showFilter}
            onRequestClose={() => {
              console.log("modal closed");
            }}>
            <View style={{ flex: 1, justifyContent: 'flex-end', marginTop: 22 }}>
              <View style={styles.modalContainer}>
                <View style={styles.modalTitle}>
                  <Text style={{ color: '#242E42', fontWeight: 'bold', fontSize: 18 }}>Filter by</Text>
                  <TouchableOpacity onPress={() => { this.setModalVisible(!this.state.showFilter); }}>
                    <Icon name='close' type='material' color='#8B9293' size={35} />
                  </TouchableOpacity>
                </View>
                <View style={styles.modalBody}>
                  <TouchableOpacity onPress={() => { this.toggleSort() }} style={styles.bodyRow}>
                    <Text style={[{ fontSize: 18, color: '#8B9293' }, this.state.isAscending ? { fontWeight: 'bold', color: '#242E42' } : null]}>Name ascending</Text>
                    {this.state.isAscending ? <Icon name='check' style={{ fontWeight: 'bold' }} type='material' color='#145CCA' size={20} /> : null}
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => { this.toggleSort() }} style={styles.bodyRow}>
                    <Text style={[{ fontSize: 18, color: '#8B9293' }, this.state.isAscending ? null : { fontWeight: 'bold', color: '#242E42' }]}>Name descending</Text>
                    {this.state.isAscending ? null : <Icon name='check' style={{ fontWeight: 'bold' }} type='material' color='#145CCA' size={20} />}
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

        </View>
      </Layout>
    )
  }
}

const mapStateToProps = state => {
  return {
    sesionOneToOneDetailsSuccess: state.homeReducer.sesionOneToOneDetailsSuccess,
    user_name: state.loginReducer.user_name,
    user_id: state.loginReducer.user_id,
    member_entity: state.loginReducer.member_entity,
    role: state.loginReducer.role,
    sessionRequest: state.homeReducer.sessionRequest,
    endUsers: state.homeReducer.endUsers,
    getEndUsersSuccess: state.homeReducer.getEndUsersSuccess
  }
}

const mapDispatchToProps = {
  getAllEndUsers,
  setLocation,
  createOneToOneCallSession,
  setCallInActive,
  setCallActive,
  registerFCMToken
}

export default connect(mapStateToProps, mapDispatchToProps)(ListFieldExecutives)
