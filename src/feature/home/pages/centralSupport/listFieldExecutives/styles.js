import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15,
  },
  tileContainer: {
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  txtName: {
    color: '#fff',
    fontSize: 14,
    fontWeight: '500'
  },
  txtUserName: {
    color: '#fff',
    fontSize: 12,
    marginTop: 5
  },
  modalContainer: {
    backgroundColor: '#fff',
    height: '50%'
  },
  modalTitle: {
    flexDirection: 'row',
    justifyContent: "space-between",
    alignItems: 'center',
    padding: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc'
  },
  modalBody: {
    padding: 20,
  },
  bodyRow: {
    paddingBottom: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
})

export default styles
