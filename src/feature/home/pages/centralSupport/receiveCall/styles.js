import { StyleSheet, Dimensions } from 'react-native'
const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft: 15,
    paddingRight: 15
  },
  tileContainer: {
    borderColor: '#fff',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    marginBottom: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  txtFrom: {
    color: '#fff'
  },
  bottomButtonOuter: {
    position: 'absolute',
    bottom: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  publisherContainerPotrait: { flex: 1 },
  publisherContainerLandscape: {},
  messageConatiner: {
    width: DEVICE_WIDTH,
    height: 200,
    position: 'absolute',
    bottom: 0,
    borderTopColor: '#eee',
    borderTopWidth: 2,
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: '#fff'
  },
  bubbleOuter: {
    width: DEVICE_WIDTH,
    display: 'flex'
  },
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#F5FCFF'
  // },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5
  },
  bubbleInner: {
    width: DEVICE_WIDTH - 50,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: '#eee',
    margin: 5,
    marginRight: 20
  },
  messageFooterContainer: {
    width: DEVICE_WIDTH,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff'
  },
  sendBubble: {
    width: DEVICE_WIDTH - 20,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: '#2196f3',
    margin: 10,
    marginTop: 20,
    marginBottom: 20,
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40
  },
  sendButton: {
    // borderWidth: 1,
    // backgroundColor: "#2196f3",
    // borderColor: "rgba(0,0,0,0.2)",
    alignItems: 'center',
    justifyContent: 'center',
    // height: 40,
    // width: 70,
    // borderRadius: 50,
    flex: 1
  },
  backgroundImg: {
    // flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%'
  },
  backgroundI: { width: 40, height: 40, margin: 20 },
  backgroundIe: {
    width: 40,
    height: 40,
    position: 'absolute',
    right: 0,
    margin: 20
  },
  callContainer: {
    flex: 6,
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '30%'
  },
  callImag: { width: 30, height: 30 },
  callTextOuter: {
    width: '60%',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 40,
    justifyContent: 'center',
    borderRadius: 5,
    marginTop: 20,
    shadowColor: '#E0E0E0',
    shadowOffset: {
      width: 4,
      height: 6
    },
    shadowRadius: 2.81,
    elevation: 8
  },
  driverViewContainer: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eee'
  },
  bottomButtonOuter: {
    position: 'absolute',
    width: '100%',
    bottom: 10,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalOuter: {
    width: '90%',
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 4
  },
  settings: {
    flex: 1,
    padding: 10,
    width: '100%'
  },
  messageModal: {
    height: 250,
    position: 'absolute',
    bottom: 60,
    width: DEVICE_WIDTH
    // backgroundColor: "#fff"
  },
  messageModalcontainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
  },
  messagemodalOuter: {
    margin: 10,
    padding: 10,
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 4,
    zIndex: 9999
  },
  messageModalout: { position: 'absolute', bottom: 0 },
  topLine: {
    borderBottomWidth: 1,
    borderBottomColor: '#9e9e9e',
    // position: "absolute",
    height: 10,
    // backgroundColor: 'transparent',
    width: '100%'
  },
  closeMessage: { position: 'absolute', left: 20, top: 0, zIndex: 999 },
  closechat: { position: 'absolute', right: 20, top: 0, zIndex: 999 },
  closeBotton: {
    width: 20,
    height: 20,
    backgroundColor: '#2196f3',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  expandeMessage: { position: 'absolute', left: 20, top: -5, zIndex: 999 },
  expandeButton: {
    width: 20,
    height: 20,
    backgroundColor: '#9e9e9e',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  bubbleTop: { zIndex: 1, top: 10, backgroundColor: '#fff', height: '100%' },
  inputOuter: { padding: 10, color: '#000', flex: 5 },
  borderGrey: { borderTopColor: '#9e9e9e' },
  borderWhite: { borderTopColor: '#fff' },
  sendBubble2: {
    width: DEVICE_WIDTH - 30,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: '#2196f3',
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 40,
    marginBottom: 5,
    marginLeft: 10
  },

  clientName: {
    backgroundColor: '#000',
    padding: 10
  },
  txtClient: {
    color: '#fff'
  },
  clientsWrap: {
    backgroundColor: '#010101',
    padding: 10
  },
  selectClient: {
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    paddingBottom: 10,
    paddingTop: 10
  },
  btnCallEnd: {
    marginLeft: 15,
    marginRight: 15,
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red',
    borderRadius: 25
  },
  btnMuteAudio: {
    marginLeft: 15,
    marginRight: 15,
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25
  },
  btnActive: {
    backgroundColor: 'green',
  },
  btnInActive: {
    backgroundColor: '#ccc',
  },
  btnMuteVideoActive: {
    marginLeft: 15,
    marginRight: 15,
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green',
    borderRadius: 25
  },
  btnShowMessage: {
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'blue',
    borderRadius: 25
  }
})

export default styles
