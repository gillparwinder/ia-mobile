import React, { Component } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,
  NativeModules,
  BackHandler,
  TextInput,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  Modal,
  NativeEventEmitter,
  Platform,
  KeyboardAvoidingView
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import { Icon } from 'react-native-elements'

import firebase from 'react-native-firebase'
import Orientation from 'react-native-orientation'
import Config from 'react-native-config'
import { APPID } from '../../agoraSettings'
import { RtcEngine, AgoraView } from 'react-native-agora'
import _ from 'lodash'
import RNCallKeep from 'react-native-callkeep';

import {
  connectCall,
  disconnectSession,
  resetAgora,
  changeCamera,
  changeDisplayMode,
  newMessage,
  translateFn,
  setCallInActive,
  sendJobNumber
} from '../../../actions/actions'
import moment from 'moment'
import KeepAwake from 'react-native-keep-awake'
import RenderMessage from '../../../../components/renderMessage'
import ReactNativeInvokeApp from '../../../../components/invokeApp'
const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height


const { Agora } = NativeModules
const {
  FPS15,
  FPS30,
  FixedLandscape,
  FixedPortrait,
  Adaptative,
  AudioProfileDefault,
  AudioScenarioDefault,
  Host,
  CapturerOutputPreferenceAuto
} = Agora

const firebaseConfig = {
  apiKey: Config.API_KEY,
  authDomain: Config.AUTH_DOMAIN,
  databaseURL: Config.DATABASE_URL,
  projectId: Config.PROJECT_ID,
  storageBucket: Config.STORAGE_BUCKET,
  messagingSenderId: Config.MESSAGING_SENDER_ID,
  appId: Config.APP_ID
}

let conversationRef = ''
let sessionRef = ''

class ReceiveCall extends Component {
  constructor(props) {
    super(props)
    this.state = {
      orientation: 'POTRAIT',
      showmessage: false,
      messageModal: false,
      peerIds: [],
      incomingCalls: [],
      joinSucceed: false,
      firebase_key: '',
      signals: [],
      messages: [],
      connected: false,
      attended: false,
      isAudioMuted: false,
      callEnding: false,
      modalVisible: false,
      jobNumber: '',
      isError: false,
      errorMessage: '',
      callEndingBy: "",
    }
  }

  componentWillMount() {
    KeepAwake.activate();
    const config = {
      appid: APPID,
      channelProfile: 1,
      videoProfile: this.props.videoProfile,
      clientRole: Host,

      frameRate: FPS30,
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault,
      keepPrerotation: false,
      cameraIndex: 1025
    };
    console.log("[CONFIG]", JSON.stringify(config));
    RtcEngine.on('firstRemoteVideoDecoded', data => {
      console.log('[RtcEngine] onFirstRemoteVideoDecoded', data)
    });
    RtcEngine.on("userJoined", data => {
      console.log("[RtcEngine] onUserJoined", data);
      const { peerIds } = this.state;
      if (peerIds.indexOf(data.uid) === -1) {
        this.setState({
          peerIds: [...peerIds, data.uid]
        });
      }
    });
    RtcEngine.on("userOffline", data => {
      console.log("[RtcEngine] onUserOffline", data);
      this.disconnectSession();
      this.setState({
        joinSucceed: false
      });
    });
    RtcEngine.on("joinChannelSuccess", data => {
      console.log("[RtcEngine] onJoinChannelSuccess", data);
      this.setState({
        joinSucceed: true,
        animating: false
      });
      this.props.connectCall(this.props.navigation.getParam('firebaseKey', '')
      );
    });
    RtcEngine.on('clientRoleChanged', data => {
      console.log('[RtcEngine] onClientRoleChanged', data)
    })
    RtcEngine.on('error', data => {
      console.log('[RtcEngine] onError', data)
      if (data.error === 17) {
        this.props.disconnectSession(this.props.navigation.getParam('firebaseKey', ''))
        Orientation.lockToPortrait()
        this.props.resetAgora()
        //  this.goBack();
      }
    })
    RtcEngine.init(config)
  }

  componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackPress
    )
    if (!firebase.apps.length) {
      firebase.initializeApp(firebaseConfig);
    }

    RtcEngine.joinChannel(this.props.navigation.getParam('sessionId', ''), Math.floor(Math.random() * 100))
    this.setState({ firebase_key: this.props.navigation.getParam('firebaseKey', '') })

    sessionRef = firebase
      .database()
      .ref("ionassist-root/sessions/" + this.props.member_entity + "/" + this.props.navigation.getParam('firebaseKey', ''))

    conversationRef = firebase
      .database()
      .ref('ionassist-root/sessions_conversation/' + this.props.member_entity + '/' +
        this.props.navigation.getParam('sessionId', '')
      )

    sessionRef.on("child_changed", snapshot => {
      if (snapshot.val() === "DISCONNECTED") {
        this.disconnectIosCall();
        this.props.resetAgora()
        this.setState({ callEnding: true, joinSucceed: false });
        if (this.state.callEndingBy != "you") {
          this.setState({ callEndingBy: "peer" });
          this.setJobNumberModelVisible(true);
        }
      }
    });

    conversationRef.on('child_added', snapshot => {
      let incomingMessages = snapshot.val()
      if (
        this.props.language &&
        incomingMessages.language_code &&
        incomingMessages.language_code !== this.props.language
      ) {
        this.props.translateFn(
          incomingMessages.language_code,
          this.props.language,
          incomingMessages
        )
      } else this.props.newMessage(incomingMessages)
      this.setState({ showmessage: true, messageModal: true })
      Orientation.lockToPortrait();
      if (this.state.orientation === '"LANDSCAPE"') {
        this.setState({ orientation: 'POTRAIT' })
        if (this.props.sesionDetails.id) { this.props.changeDisplayMode(this.props.sesionDetails.id, 'POTRAIT') }
        Orientation.lockToPortrait()
      }
      this._scrollView ? this._scrollView.scrollToEnd({ animated: true }) : null
      this.secondTextInput ? this.secondTextInput.focus() : null
    })
  }

  componentDidUpdate(nextProps) {
    if (this.props.sendJobNumberSuccess != nextProps.sendJobNumberSuccess && this.props.sendJobNumberSuccess) {
      this.setJobNumberModelVisible(false);
      this.goBack();
    }
    else if (this.props.sendJobNumberFailed != nextProps.sendJobNumberFailed && this.props.sendJobNumberFailed) {
      this.setState({ isError: true, errorMessage: 'Something went wrong in sending job number.' })
    }
  }

  componentWillUnmount() {
    KeepAwake.deactivate();
    RtcEngine.destroy();
    RtcEngine.removeAllListeners();
    this.backHandler.remove();
  }

  goBack = () => {
    this.props.setCallInActive();
    Orientation.lockToPortrait();
    if (Platform.OS == 'android') {
      ReactNativeInvokeApp.detectLock();
      const eventEmitter = new NativeEventEmitter(NativeModules.ReactNativeInvokeApp);
      eventEmitter.addListener('phoneLocked', (data) => {
        RtcEngine.leaveChannel()
          .then(_ => {
            this.props.navigation.navigate('ListFieldExecutives');
            data.isLocked == "locked" ? BackHandler.exitApp() : null;
          })
          .catch(err => {
            console.log('[agora]: err', err)
          })
      });
    }
    else {
      RtcEngine.leaveChannel()
        .then(_ => {
          this.props.navigation.navigate('ListFieldExecutives');
        })
        .catch(err => {
          console.log('[agora]: err', err)
        })
    }
  }

  handleBackPress = () => {
    this.disconnectSession();
    return true;
  }

  disconnectIosCall = () => {
    if (Platform.OS == 'ios')
      RNCallKeep.endCall(this.props.currentCallInfo.callUUID);
  }

  disconnectSession = () => {
    this.disconnectIosCall();
    this.setState({ callEndingBy: "you", callEnding: true })
    this.props.disconnectSession(this.props.navigation.getParam('firebaseKey', ''));
    this.setJobNumberModelVisible(true);
  };

  setJobNumberModelVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  submitJobNumber() {
    if (this.state.jobNumber.length == 0)
      this.setState({ isError: true, errorMessage: '*enter valid job number.' })
    else
      this.props.sendJobNumber(this.state.jobNumber, this.props.navigation.getParam('firebaseKey', ''));
  }

  showmessagefn = () => {
    Orientation.lockToPortrait()
    conversationRef.once('value', snapshot => {
      if (this.props.signals.length == 0 && snapshot.val()) { this.props.newMessage(snapshot.val()) }
    })
    this.setState({ showmessage: true, messageModal: true })
  }

  setmessageVisible(visible) {
    this.setState({ messageModal: visible });
  }

  sendSignal = message => {
    let sendingObj = {
      message_type: 'text',
      payload: message,
      author: this.props.user_id,
      author_name: this.props.user_name,
      created_on: moment.utc().valueOf(),
      language_code: this.props.language
    }

    conversationRef
      .push(sendingObj)
      .then(data => {
        this.setState({
          message: ''
        })
        this._scrollView.scrollToEnd({ animated: true })
      })
      .catch(error => {
        // error callback
        console.log('error ', error)
      })
  }

  toggleMuteAudio = () => {
    RtcEngine.muteLocalAudioStream(!this.state.isAudioMuted);
    this.setState({ isAudioMuted: !this.state.isAudioMuted });
  }

  renderCallInfo = () => {
    return (
      <View style={styles.bottomButtonOuter}>
        <View style={{ display: 'flex', flexDirection: 'row' }}>
          <TouchableOpacity onPress={() => this.toggleMuteAudio()}>
            <View style={[styles.btnMuteAudio, this.state.isAudioMuted ? styles.btnInActive : styles.btnActive]}>
              {this.state.isAudioMuted ?
                <Icon name='mic-off' type='material' color='#fff' size={20} /> :
                <Icon name='mic-none' type='material' color='#fff' size={20} />
              }
            </View>
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.disconnectSession()}>
            <View style={styles.btnCallEnd}>
              <Icon name='call-end' type='material' color='#fff' size={20} />
            </View>
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={{ position: 'absolute', right: 10 }} onPress={() => this.showmessagefn()}>
          <View style={styles.btnShowMessage}>
            <Icon name='message' type='material' color='#fff' size={20} />
          </View>
        </TouchableOpacity>
      </View>

    );
  };

  closeChat = () => {
    this.setState({ showmessage: false, messageModal: false })
    Orientation.unlockAllOrientations()
  }

  renderMessage = () => {
    const borderTopColormodal = !this.state.messageModal
      ? styles.borderGrey
      : styles.borderWhite
    return (
      <View style={styles.messageModalout}>
        {this.state.messageModal ? (
          <View style={styles.messageModal}>
            <View style={styles.topLine} />
            <TouchableHighlight
              style={styles.closeMessage}
              onPress={() => this.setmessageVisible(false)}
            >
              <View style={styles.closeBotton}>
                <Icon name='expand-more' type='material' color='#fff' size={20} />
              </View>
            </TouchableHighlight>
            <TouchableHighlight
              style={styles.closechat}
              onPress={() => this.closeChat()}
            >
              <View style={styles.closeBotton}>
                <Icon name='close' type='material' color='#fff' size={20} />
              </View>
            </TouchableHighlight>
            <View style={{ backgroundColor: '#fff', height: 240, width: '100%' }}>
              <TouchableWithoutFeedback style={styles.messageModalcontainer}>
                <View style={styles.messagemodalOuter}>
                  <ScrollView
                    style={{ width: DEVICE_WIDTH, height: '100%' }}
                    ref={ref => {
                      this._scrollView = ref
                    }}
                  >
                    {this.props.signals && this.props.signals.length > 0
                      ? this.props.signals.map((item, index) => {
                        return (
                          <RenderMessage key={index} index={index} item={item} user_id={this.props.user_id}></RenderMessage>
                        )
                      })
                      : null}
                  </ScrollView>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        ) : null}
        <View style={{ flex: 1 }}>
          <View style={styles.messageFooterContainer}>
            {!this.state.messageModal ? (
              <TouchableOpacity
                style={styles.expandeMessage}
                onPress={() => this.setmessageVisible(true)}
              >
                <View style={styles.expandeButton}>
                  <Icon
                    name='expand-less'
                    type='material'
                    color='#fff'
                    size={20}
                  />
                </View>
              </TouchableOpacity>
            ) : null}
            <View style={[styles.bubbleTop, borderTopColormodal]}>
              <View style={styles.sendBubble}>
                <TextInput
                  ref={input => {
                    this.secondTextInput = input
                  }}
                  style={styles.inputOuter}
                  value={this.state.message}
                  onChangeText={message => this.setState({ message })}
                />
                <TouchableOpacity
                  style={styles.sendButton}
                  onPress={() => this.sendSignal(this.state.message)}
                >
                  <Icon
                    name='arrow-forward'
                    type='material'
                    color='#9e9e9e'
                    size={30}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </View>
    )
  }

  render() {
    return <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS == "ios" ? "padding" : "height"}>{this.state.joinSucceed ? (
      this.props.disableSessionRequest || this.state.callEnding ?
        <View style={{ display: 'flex', alignItems: 'center', junullstifyContent: 'center' }}>
          <Text>Ending Call...</Text>
        </View> :
        <View style={{ flex: 1, backgroundColor: "#fff", junullstifyContent: "center", alignItems: "center" }} >
          {this.state.peerIds.map(uid => {
            return (<AgoraView mode={1} key={uid} style={{ width: "100%", height: "100%" }} remoteUid={uid} />);
          })}
          {/* TODO: This was giving issue... Need to check for its use. */}
          {/* zOrderMediaOverlay={true} */}
          {this.state.showmessage ? this.renderMessage() : this.renderCallInfo()}
        </View>
    ) :
      <View style={{ display: 'flex', alignItems: 'center', junullstifyContent: 'center' }}>
        <Text>Connecting...</Text>
      </View>
    }

      < Modal animationType="slide" transparent={true} visible={this.state.modalVisible} >
        <View style={{ flex: 1, justifyContent: "center", alignItems: 'center', backgroundColor: "rgba(40, 44, 52, 0.5)" }}>
          <View style={{ width: "90%", height: 250, backgroundColor: '#fff', borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
            <TextInput onChangeText={text => this.setState({ jobNumber: text })} value={this.state.jobNumber} placeholder="Enter job number" style={{ paddingLeft: 10, height: 40, width: '80%', backgroundColor: '#dedede', borderWidth: 1, borderColor: '#3b4c69', borderRadius: 5 }}></TextInput>
            <TouchableOpacity style={{ height: 40, width: 200, alignItems: 'center', justifyContent: 'center', marginTop: 50, paddingLeft: 20, paddingRight: 20, borderRadius: 5, backgroundColor: '#3b4c69' }}
              onPress={() => { this.submitJobNumber() }}>
              <Text style={{ color: '#fff' }}>Submit</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal >

      {
        this.props.processingRequest ?
          <View style={{ position: 'absolute', zIndex: 100, height: '100%', width: '100%', justifyContent: "center", alignItems: "center" }} >
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
          : null
      }
    </KeyboardAvoidingView>
  }
}
const mapStateToProps = state => {
  return {
    count: state.homeReducer.count,
    sesionDetails: state.homeReducer.sesionDetails,
    sesionDetailsSuccess: state.homeReducer.sesionDetailsSuccess,
    disableSessionRequest: state.homeReducer.disableSessionRequest,
    loggedIn: state.loginReducer.loggedIn,
    language: state.loginReducer.language,
    user_name: state.loginReducer.user_name,
    user_id: state.loginReducer.user_id,
    member_entity: state.loginReducer.member_entity,
    sessionRequest: state.homeReducer.sessionRequest,
    camera: state.homeReducer.camera,
    signals: state.homeReducer.signals,
    sendJobNumberSuccess: state.homeReducer.sendJobNumberSuccess,
    sendJobNumberFailed: state.homeReducer.sendJobNumberFailed,
    processingRequest: state.homeReducer.processingRequest,
    currentCallInfo: state.homeReducer.currentCallInfo
  }
}

const mapDispatchToProps = {
  connectCall,
  disconnectSession,
  resetAgora,
  changeCamera,
  changeDisplayMode,
  newMessage,
  translateFn,
  setCallInActive,
  sendJobNumber
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReceiveCall)
