import * as types from '../../../types/types'

const initialState = {
  processingRequest: false,
  sesionDetailsSuccess: false,
  sessionRequest: false,
  error: false,
  sesionDetails: {
    sesion_id: '',
    token: '',
    user_id: '',
    id: '',
    member_entity: '',
    user_name: '',
    firebase_id: ''
  },
  count: 0,
  camera: 'FRONT',
  display_mode: 'PORTRAIT',
  location: {},
  signals: [],
  licenceValid: true,
  licenceValidRequest: false,
  translate: {
    from: '',
    to: '',
    data: ''
  },
  endUsers: [],
  getEndUsersSuccess: false,
  sesionOneToOneDetailsSuccess: false,
  sesionOneToOneDetails: {},
  disableSessionRequest: false,
  disableSessionOneToOneRequest: false,
  disableSessionOneToOneRequestSuccess: false,
  isCallActive: false,
  sendJobNumberSuccess: false,
  sendJobNumberFailed: false,
  sendJobNumberResponse: '',
  registerFCMTokenSuccess: true,
  registerFCMTokenFailed: false,
  fcmTokenResponse: '',
  currentCallInfo: {
    callUUID: ''
  },
  recordedVideoUrl: '',
  preSignedUrl: {},
  sendRecordedVideoInfoSuccess: false,
  preSignedRequestUrlSuccess: false
}

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CREATE_CALL_SESSION_REQUEST:
      return {
        ...state,
        processingRequest: true,
        sesionDetailsSuccess: false,
        signals: [],
        error: false,
        sessionRequest: true,
        sesionDetails: {
          sesion_id: '',
          token: '',
          user_id: '',
          id: '',
          member_entity: '',
          user_name: '',
          firebase_id: ''
        }
      }

    case types.CREATE_CALL_SESSION_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        sesionDetailsSuccess: true,
        sessionRequest: false,
        error: false,
        sesionDetails: {
          sesion_id: action.payload.session_id,
          user_name: action.payload.user_name,
          user_id: action.payload.user_id,
          id: action.payload._id,
          member_entity: action.payload.member_entity,
          firebase_id: action.payload.firebase_id
        }
      }

    case types.CREATE_CALL_SESSION_FAILURE:
      return {
        ...state,
        error: true,
        processingRequest: false,
        sesionDetailsSuccess: false,
        sessionRequest: false,
        sesionDetails: {
          sesion_id: '',
          user_name: '',
          user_id: '',
          id: '',
          uniq: '',
          firebase_id: ''
        }
      }

    case types.CREATE_ONETOONE_CALL_SESSION_REQUEST:
      return {
        ...state,
        error: false,
        processingRequest: true,
        sesionOneToOneDetailsSuccess: false,
        sesionOneToOneDetails: {}
      }

    case types.CREATE_ONETOONE_CALL_SESSION_SUCCESS:
      return {
        ...state,
        error: false,
        processingRequest: false,
        sesionOneToOneDetailsSuccess: true,
        sesionOneToOneDetails: action.payload
      }

    case types.CREATE_ONETOONE_CALL_SESSION_FAILURE:
      return {
        ...state,
        error: true,
        processingRequest: false,
        sesionOneToOneDetailsSuccess: false,
        sesionOneToOneDetails: {}
      }

    case types.GET_ALL_END_USERS_REQUEST:
      return {
        ...state,
        processingRequest: true,
        getEndUsersSuccess: false,
        endUsers: [],
        error: false
      }

    case types.GET_ALL_END_USERS_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        getEndUsersSuccess: true,
        endUsers: action.payload,
        error: false
      }

    case types.GET_ALL_END_USERS_FAILURE:
      return {
        ...state,
        processingRequest: false,
        getEndUsersSuccess: false,
        endUsers: [],
        error: false
      }

    case types.SEND_JOB_NUMBER_REQUEST:
      return {
        ...state,
        processingRequest: true,
        sendJobNumberSuccess: false,
        sendJobNumberFailed: false,
        sendJobNumberResponse: ''
      }

    case types.SEND_JOB_NUMBER_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        sendJobNumberSuccess: true,
        sendJobNumberFailed: false,
        sendJobNumberResponse: action.payload
      }

    case types.SEND_JOB_NUMBER_FAILURE:
      return {
        ...state,
        processingRequest: false,
        sendJobNumberSuccess: false,
        sendJobNumberFailed: true,
        sendJobNumberResponse: ''
      }

    case types.SEND_RECORDED_VIDEO_INFO_REQUEST:
      return {
        ...state,
        processingRequest: true,
        sendRecordedVideoInfoSuccess: false
      }

    case types.SEND_RECORDED_VIDEO_INFO_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        sendRecordedVideoInfoSuccess: true
      }

    case types.SEND_RECORDED_VIDEO_INFO_FAILURE:
      return {
        ...state,
        processingRequest: false,
        sendRecordedVideoInfoSuccess: false
      }

    case types.REGISTER_FCM_TOKEN_REQUEST:
      return {
        ...state,
        processingRequest: true,
        registerFCMTokenSuccess: false,
        registerFCMTokenFailed: false,
        fcmTokenResponse: ''
      }

    case types.REGISTER_FCM_TOKEN_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        registerFCMTokenSuccess: true,
        registerFCMTokenFailed: false,
        fcmTokenResponse: action.payload
      }

    case types.REGISTER_FCM_TOKEN_FAILURE:
      return {
        ...state,
        processingRequest: false,
        registerFCMTokenSuccess: false,
        registerFCMTokenFailed: true,
        fcmTokenResponse: ""
      }

    case types.IGNORE_CALL_REQUEST:
      return {
        ...state,
        processingRequest: true,
        ignoreCallSuccess: false
      }

    case types.IGNORE_CALL_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        ignoreCallSuccess: true
      }

    case types.IGNORE_CALL_FAILURE:
      return {
        ...state,
        processingRequest: false,
        ignoreCallSuccess: false
      }

    case types.DISABLE_SESSION_REQUEST:
      return {
        ...state,
        processingRequest: true,
        disableSessionRequest: true,
        error: false
      }

    case types.DISABLE_SESSION_SUCCESS:
      return {
        ...state,
        processingRequest: false,
        sesionDetailsSuccess: false,
        disableSessionRequest: false,
        error: false,
        sesionDetails: {
          sesion_id: '',
          token: '',
          user_id: '',
          id: '',
          uniq: '',
          firebase_id: ''
        }
      }

    case types.DISABLE_SESSION_FAILURE:
      return {
        ...state,
        disableSessionRequest: false,
        error: true,
        processingRequest: false
      }


    case types.DISABLE_ONETOONE_SESSION_REQUEST:
      return {
        ...state,
        processingRequest: true,
        disableSessionOneToOneRequest: true,
        disableSessionOneToOneRequestSuccess: false,
        error: false
      }

    case types.DISABLE_ONETOONE_SESSION_SUCCESS:
      return {
        ...state,
        error: false,
        disableSessionOneToOneRequest: false,
        disableSessionOneToOneRequestSuccess: true,
        processingRequest: false,
        sesionOneToOneDetails: {}
      }

    case types.DISABLE_ONETOONE_SESSION_FAILURE:
      return {
        ...state,
        error: true,
        disableSessionOneToOneRequest: false,
        disableSessionOneToOneRequestSuccess: false,
        processingRequest: false
      }

    case types.SAVE_CURRENT_CALL_REQUEST:
      return {
        ...state,
        currentCallInfo: action.payload
      }

    case types.SET_CALL_ACTIVE:
      return {
        ...state,
        isCallActive: true
      }

    case types.SET_CALL_INACTIVE:
      return {
        ...state,
        isCallActive: false
      }

    case types.CHANGE_ORIENTATION:
      return {
        ...state,
        camera: action.payload
      }

    case types.CHANGE_DISPLAY_MODE_REQUEST:
      return {
        ...state,
        display_mode: ''
      }

    case types.CHANGE_DISPLAY_MODE_SUCCESS:
      return {
        ...state,
        display_mode: action.payload
      }

    case types.CHANGE_DISPLAY_MODE_FAILURE:
      return {
        ...state,
        display_mode: ''
      }

    case types.RESET_AGORA:
      return {
        ...state,
        error: false,
        processingRequest: false,
        sesionDetailsSuccess: false,
        sessionRequest: false,
        signals: [],
        sesionDetails: {
          sesion_id: '',
          token: '',
          user_id: '',
          id: '',
          uniq: '',
          firebase_id: ''
        }
      }

    case types.SET_LOCATION:
      return {
        ...state,
        location: {
          lat: action.payload.lat,
          lon: action.payload.lon
        }
      }

    case types.LISTEN_NEW_MESSAGE_REQUEST:
      return {
        ...state
      }

    case types.LICENCE_VALID_REQUEST:
      return {
        ...state,
        licenceValid: true,
        licenceValidRequest: true
      }

    case types.LICENCE_VALID_SUCCESS:
      return {
        ...state,
        licenceValid: action.payload.valid,
        licenceValidRequest: true
      }
    case types.LICENCE_VALID_FAILURE:
      return {
        ...state,
        licenceValid: true,
        licenceValidRequest: false
      }

    case types.GET_PRE_SIGNED_URL_REQUEST:
      return {
        ...state,
        preSignedUrl: {},
        processingRequest: true,
        preSignedRequestUrlSuccess: false
      }

    case types.GET_PRE_SIGNED_URL_SUCCESS:
      return {
        ...state,
        preSignedUrl: action.payload,
        processingRequest: false,
        preSignedRequestUrlSuccess: true
      }
    case types.GET_PRE_SIGNED_URL_FAILURE:
      return {
        ...state,
        preSignedUrl: {},
        processingRequest: false,
        preSignedRequestUrlSuccess: false

      }

    case types.NEW_MESSAGE_REQUEST:
      return {
        ...state,
        signals: [...state.signals, action.payload.data]
      }

    case types.LANGUAGE_TRANSALATION_REQUEST:
      return {
        ...state,
        translate: {
          from: action.payload.from,
          to: action.payload.to,
          message: action.payload.message
        }
      }

    case types.LANGUAGE_TRANSALATION_SUCCESS:
      return {
        ...state,
        signals: [
          ...state.signals,
          {
            message_type: state.translate.message.message_type,
            payload: action.payload.data.translations[0].translatedText,
            author: state.translate.message.author,
            author_name: state.translate.message.author_name,
            created_on: state.translate.message.created_on,
            language_name: state.translate.message.language_name,
            language_code: state.translate.message.language_code
          }
        ]
      }
    case types.LANGUAGE_TRANSALATION_FAILURE:
      return {
        ...state,
        signals: [...state.signals, state.translate.message]
      }
    case types.SET_RECORDED_VIDEO_URL:
      return {
        ...state,
        recordedVideoUrl: action.payload.videoUrl
      }
    default:
      return state
  }
}

export default homeReducer
