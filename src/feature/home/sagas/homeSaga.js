import * as types from '../../../types/types'
import * as HomeAPi from '../api/api.home'
import { callAPI } from '../../../sagas/api'
import {
  all,
  call,
  put,
  takeEvery,
  takeLatest,
  select,
  take,
  fork
} from 'redux-saga/effects'

function* createCallSession(action) {
  const response = yield callAPI(
    HomeAPi.createCallSession,
    action.payload,
    types.CREATE_CALL_SESSION_FAILURE
  )
  if (response)
    yield put({
      type: types.CREATE_CALL_SESSION_SUCCESS,
      payload: response.data
    })
}

function* createOneToOneCallSession(action) {
  const response = yield callAPI(
    HomeAPi.createOneToOneCallSession,
    action.payload,
    types.CREATE_ONETOONE_CALL_SESSION_FAILURE
  )
  if (response)
    yield put({
      type: types.CREATE_ONETOONE_CALL_SESSION_SUCCESS,
      payload: response.data
    })
}

function* disconnectSession(action) {
  const response = yield callAPI(
    HomeAPi.disconnectSession,
    action.payload,
    types.DISABLE_SESSION_FAILURE
  )
  if (response)
    yield put({ type: types.DISABLE_SESSION_SUCCESS, payload: response.data })
}

function* disconnectOneToOneSession(action) {
  const response = yield callAPI(
    HomeAPi.disconnectOneToOneSession,
    action.payload,
    types.DISABLE_ONETOONE_SESSION_FAILURE
  )
  if (response)
    yield put({ type: types.DISABLE_ONETOONE_SESSION_SUCCESS, payload: response.data })
}

function* changeDisplayMode(action) {
  const response = yield callAPI(
    HomeAPi.changeDisplayMode,
    action.payload,
    types.CHANGE_DISPLAY_MODE_FAILURE
  )
  if (response)
    yield put({
      type: types.CHANGE_DISPLAY_MODE_SUCCESS,
      payload: response.data
    })
}

export function* listenToIncomingMessage(action) {
  const incomingMessageEventChannel = HomeAPi.incomingMessageEventChannel(
    action.payload
  )
  yield fork(closeIncomingMessageEventChannel, incomingMessageEventChannel)
  while (true) {
    const item = yield take(incomingMessageEventChannel)
    yield put({ type: types.LISTEN_NEW_MESSAGE_SUCCESS, payload: item })
  }
}

function* closeIncomingMessageEventChannel(incomingMessageEventChannel) {
  while (true) {
    yield take(types.LISTEN_NEW_MESSAGE_CANCELLED)
    incomingMessageEventChannel.close()
  }
}

function* getLicenceValid(action) {
  const response = yield callAPI(
    HomeAPi.getLicenceValid,
    action.payload,
    types.LICENCE_VALID_FAILURE
  )
  if (response)
    yield put({ type: types.LICENCE_VALID_SUCCESS, payload: response.data })
}

function* translateData(action) {
  const response = yield callAPI(
    HomeAPi.translateData,
    action.payload,
    types.LANGUAGE_TRANSALATION_FAILURE
  )
  if (response)
    yield put({
      type: types.LANGUAGE_TRANSALATION_SUCCESS,
      payload: response.data
    })
}

function* getAllEndUsers(action) {
  const response = yield callAPI(
    HomeAPi.getAllEndUsers,
    action.payload,
    types.GET_ALL_END_USERS_FAILURE
  )
  if (response) {
    yield put({
      type: types.GET_ALL_END_USERS_SUCCESS,
      payload: response.data
    })
  }
}

function* connectCall(action) {
  const response = yield callAPI(
    HomeAPi.connectCall,
    action.payload,
    types.CONNECT_CALL_FAILURE
  )
  if (response)
    yield put({
      type: types.CONNECT_CALL_SUCCESS,
      payload: response.data
    })
}

function* connectOneToOneCall(action) {
  const response = yield callAPI(
    HomeAPi.connectOneToOneCall,
    action.payload,
    types.CONNECT_ONETOONE_CALL_FAILURE
  )
  if (response)
    yield put({
      type: types.CONNECT_ONETOONE_CALL_SUCCESS,
      payload: response.data
    })
}


function* sendJobNumber(action) {
  const response = yield callAPI(
    HomeAPi.sendJobNumber,
    action.payload,
    types.SEND_JOB_NUMBER_FAILURE
  )
  if (response)
    yield put({
      type: types.SEND_JOB_NUMBER_SUCCESS,
      payload: response.data
    })
}

function* registerFCMToken(action) {
  const response = yield callAPI(
    HomeAPi.registerFCMToken,
    action.payload,
    types.REGISTER_FCM_TOKEN_FAILURE
  )
  if (response)
    yield put({
      type: types.REGISTER_FCM_TOKEN_SUCCESS,
      payload: response.data
    })
}

function* ignoreCall(action) {
  const response = yield callAPI(
    HomeAPi.ignoreCall,
    action.payload,
    types.IGNORE_CALL_FAILURE
  )
  if (response)
    yield put({
      type: types.IGNORE_CALL_SUCCESS,
      payload: response.data
    })
}

function* getPreSignedUrlRequest(action) {
  const response = yield callAPI(
    HomeAPi.getPreSignedUrlRequest,
    action.payload,
    types.GET_PRE_SIGNED_URL_FAILURE
  )
  if (response)
    yield put({
      type: types.GET_PRE_SIGNED_URL_SUCCESS,
      payload: response.data
    })
}

function* sendRecordedVideoInfo(action) {
  const response = yield callAPI(
    HomeAPi.sendRecordedVideoInfo,
    action.payload,
    types.SEND_RECORDED_VIDEO_INFO_FAILURE
  )
  if (response)
    yield put({
      type: types.SEND_RECORDED_VIDEO_INFO_SUCCESS,
      payload: response.data
    })
}

export function* watchHome() {
  yield takeEvery(types.CREATE_CALL_SESSION_REQUEST, createCallSession)
  yield takeEvery(types.DISABLE_SESSION_REQUEST, disconnectSession)
  yield takeEvery(types.CHANGE_DISPLAY_MODE_REQUEST, changeDisplayMode)
  yield takeEvery(types.LISTEN_NEW_MESSAGE_REQUEST, listenToIncomingMessage)
  yield takeEvery(types.LICENCE_VALID_REQUEST, getLicenceValid)
  yield takeEvery(types.LANGUAGE_TRANSALATION_REQUEST, translateData)
  yield takeEvery(types.CONNECT_CALL_REQUEST, connectCall)
  yield takeEvery(types.GET_ALL_END_USERS_REQUEST, getAllEndUsers)
  yield takeEvery(types.CREATE_ONETOONE_CALL_SESSION_REQUEST, createOneToOneCallSession)
  yield takeEvery(types.DISABLE_ONETOONE_SESSION_REQUEST, disconnectOneToOneSession)
  yield takeEvery(types.CONNECT_ONETOONE_CALL_REQUEST, connectOneToOneCall)
  yield takeEvery(types.SEND_JOB_NUMBER_REQUEST, sendJobNumber)
  yield takeEvery(types.REGISTER_FCM_TOKEN_REQUEST, registerFCMToken)
  yield takeEvery(types.IGNORE_CALL_REQUEST, ignoreCall)
  yield takeEvery(types.GET_PRE_SIGNED_URL_REQUEST, getPreSignedUrlRequest)
  yield takeEvery(types.SEND_RECORDED_VIDEO_INFO_REQUEST, sendRecordedVideoInfo)
}
