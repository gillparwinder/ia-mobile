import * as types from '../../../types/types'

export function createCallSession(device_info, location) {
  return {
    type: types.CREATE_CALL_SESSION_REQUEST,
    payload: {
      device_info: device_info,
      location: location
    }
  }
}

export function createOneToOneCallSession(device_info, location, toUser) {
  return {
    type: types.CREATE_ONETOONE_CALL_SESSION_REQUEST,
    payload: {
      device_info: device_info,
      location: location,
      toUser: toUser
    }
  }
}

export function disconnectSession(id) {
  return {
    type: types.DISABLE_SESSION_REQUEST,
    payload: {
      id: id
    }
  }
}

export function disconnectOneToOneSession(id) {
  return {
    type: types.DISABLE_ONETOONE_SESSION_REQUEST,
    payload: {
      id: id
    }
  }
}

export function changeCamera(camera) {
  return {
    type: types.CHANGE_CAMERA,
    payload: {
      camera: camera
    }
  }
}

export function changeDisplayMode(id, displayMode) {
  return {
    type: types.CHANGE_DISPLAY_MODE_REQUEST,
    payload: {
      id: id,
      displayMode: displayMode
    }
  }
}

export function resetAgora() {
  return {
    type: types.RESET_AGORA
  }
}

export function setLocation(lon, lat) {
  return {
    type: types.SET_LOCATION,
    payload: {
      lon: lon,
      lat: lat
    }
  }
}

export function listenToIncomingMessage(member_entity, sesion_id) {
  return {
    type: types.LISTEN_NEW_MESSAGE_REQUEST,
    payload: {
      sesion_idchannelId: sesion_id,
      member_entity: member_entity
    }
  }
}

export function cancelListenToIncomingMessage(member_entity, sesion_id) {
  return {
    type: types.LISTEN_NEW_MESSAGE_CANCELLED,
    payload: {
      sesion_idchannelId: sesion_id,
      member_entity: member_entity
    }
  }
}

export function getLicenceValid(member_entity) {
  return {
    type: types.LICENCE_VALID_REQUEST,
    payload: {
      member_entity: member_entity
    }
  }
}

export function translateFn(from, to, message) {
  return {
    type: types.LANGUAGE_TRANSALATION_REQUEST,
    payload: {
      from: from,
      to: to,
      message: message
    }
  }
}

export function newMessage(data) {
  return {
    type: types.NEW_MESSAGE_REQUEST,
    payload: {
      data: data
    }
  }
}

export function connectCall(firebaseid) {
  return {
    type: types.CONNECT_CALL_REQUEST,
    payload: {
      firebaseid: firebaseid
    }
  }
}

export function connectOneToOneCall(firebaseid) {
  return {
    type: types.CONNECT_ONETOONE_CALL_REQUEST,
    payload: {
      firebaseid: firebaseid
    }
  }
}

export function getAllEndUsers(entityId) {
  return {
    type: types.GET_ALL_END_USERS_REQUEST,
    payload: {
      entityId: entityId
    }
  }
}

export function setCallActive() {
  return {
    type: types.SET_CALL_ACTIVE
  }
}

export function setCallInActive() {
  return {
    type: types.SET_CALL_INACTIVE
  }
}

export function sendJobNumber(jobnumber, firebaseid) {
  return {
    type: types.SEND_JOB_NUMBER_REQUEST,
    payload: {
      jobnumber: jobnumber,
      firebaseid: firebaseid
    }
  }
}

export function registerFCMToken(device_id, fcm_token) {
  return {
    type: types.REGISTER_FCM_TOKEN_REQUEST,
    payload: {
      device_id: device_id,
      fcm_token: fcm_token
    }
  }
}

export function ignoreCall(firebaseId) {
  return {
    type: types.IGNORE_CALL_REQUEST,
    payload: {
      firebaseId: firebaseId
    }
  }
}

export function saveCurrentCall(callUUID) {
  return {
    type: types.SAVE_CURRENT_CALL_REQUEST,
    payload: {
      callUUID: callUUID
    }
  }
}

export function setRecordedVideoUrl(videoUrl) {
  return {
    type: types.SET_RECORDED_VIDEO_URL,
    payload: {
      videoUrl: videoUrl
    }
  }
}

export function getPreSignedUrlRequest(fileName, ContentType) {
  return {
    type: types.GET_PRE_SIGNED_URL_REQUEST,
    payload: {
      fileName: fileName,
      ContentType: ContentType
    }
  }
}

export function sendRecordedVideoInfo(fileName, path, recordStartTime, recordEndTime, latitude, longitude) {
  return {
    type: types.SEND_RECORDED_VIDEO_INFO_REQUEST,
    payload: {
      file_name: fileName,
      path: path,
      record_start_time: recordStartTime,
      record_end_time: recordEndTime,
      latitude: latitude,
      longitude: longitude
    }
  }
}
