import { put, takeEvery, all } from "redux-saga/effects";
import * as types from "../../../types/types";
import * as LoginAPI from "../api/api.login";
import { callAPI } from "../../../sagas/api";

function* login(action) {
  const response = yield callAPI(
    LoginAPI.login,
    action.payload,
    types.LOGIN_FAILURE
  );
  if (response) {
    const response_token = yield callAPI(
      LoginAPI.getAccessToken,
      { refresh_token: response.data.refresh_token },
      types.ACCESS_TOKEN_FAILURE
    );
    // if (response_token && response_token.data.role === 'FIELD_SUPPORT_EXECUTIVE'){
    yield put({
      type: types.ACCESS_TOKEN_SUCCESS,
      payload: response_token.data
    });
    yield put({ type: types.LOGIN_SUCCESS, payload: response.data });
    // }
    // else
    //     yield put({ type: types.INVALID_ROLE, payload: response_token.data });
  }
}

function* forgotPassword(action) {
  const response = yield callAPI(
    LoginAPI.forgotPassword,
    action.payload,
    types.FORGOT_PASSWORD_FAILURE
  );
  if (response)
    yield put({ type: types.FORGOT_PASSWORD_SUCCESS, payload: response.data });
}

function* getMememerEneties(action) {
  const response = yield callAPI(
    LoginAPI.getMememerEneties,
    action.payload,
    types.GET_MEMBER_ENTITIES_FAILURE
  );
  if (response)
    yield put({
      type: types.GET_MEMBER_ENTITIES_SUCCESS,
      payload: response.data
    });
}

function* validateGuestUser(action) {
  const response = yield callAPI(
    LoginAPI.validateGuestUser,
    action.payload,
    types.VALIDATE_GUEST_USER_FAILURE
  );
  if (response)
    yield put({
      type: types.VALIDATE_GUEST_USER_SUCCESS,
      payload: response.data
    });
}

function* resetPassword(action) {
  const response = yield callAPI(
    LoginAPI.resetPassword,
    action.payload,
    types.RESET_PASSWORD_FAILURE
  );
  if (response)
    yield put({
      type: types.RESET_PASSWORD_SUCCESS,
      payload: response.data
    });
}

export function* watchLogin() {
  yield takeEvery(types.LOGIN_REQUEST, login);
  yield takeEvery(types.FORGOT_PASSWORD_REQUEST, forgotPassword);
  yield takeEvery(types.GET_MEMBER_ENTITIES_REQUEST, getMememerEneties);
  yield takeEvery(types.VALIDATE_GUEST_USER_REQUEST, validateGuestUser);
  yield takeEvery(types.RESET_PASSWORD_REQUEST, resetPassword);
}
