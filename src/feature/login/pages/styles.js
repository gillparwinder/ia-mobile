import { StyleSheet, Dimensions } from "react-native";

const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;

const styles = StyleSheet.create({
  buttoncontainer: {
    width: "90%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "blue",
    height: 40
  },
  text: {
    textAlign: "center",
    height: 40,
    color: "white",
    paddingTop: 10
  },
  loginConatiner: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#232d42",
    height: DEVICE_HEIGHT
  },
  resetPasswordSuccessContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#232d42",
  },
  backToginButton: {
    width: "90%",
    backgroundColor: "#3b4c69",
    marginLeft: 15,
    borderRadius: 5,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  logoConatiner: {
    flex: 2,
    justifyContent: "flex-end",
    display: "flex",
    flexDirection: "column"
  },
  loginText: {
    color: "#fff",
    fontWeight: "100",
    fontSize: 20
  },
  resetSuccssText: {
    color: "#fff",
    fontWeight: "100",
    fontSize: 16,
    textAlign: 'center'
  },
  textContainer: {
    flex: 2,
    justifyContent: "center",
    width: "100%",
    alignItems: "center"
  },
  emailConatiner: { display: "flex", flexDirection: "row" },
  emailOuter: {
    width: "60%"
  },
  emailInput: {
    height: 40,
    width: "90%",
    marginBottom: 20,
    backgroundColor: "#3b4c69",
    color: "#fff",
    borderRadius: 5,
    paddingLeft: 10
  },
  passwordConatier: { display: "flex", flexDirection: "row" },
  passwordOuter: { width: "60%" },
  passwordInput: {
    height: 40,
    width: "90%",
    // marginBottom: 20,
    backgroundColor: "#3b4c69",
    color: "#fff",
    borderRadius: 5,
    paddingLeft: 10
  },
  forgotContainer: {
    justifyContent: "flex-start",
    flexDirection: "column",
    width: "60%"
  },
  forgottext: {
    textAlign: "left",
    paddingLeft: 20,
    fontStyle: "italic",
    fontSize: 12,
    color: "#747a88",
    paddingTop: 5
  },
  errorText: {
    color: "red",
    fontStyle: "italic",
    fontSize: 12,
    paddingLeft: 20
  },
  buttonouter: { width: "60%", flex: 1 },
  loginButton: {
    width: "90%",
    backgroundColor: "#3b4c69",
    marginLeft: 15,
    borderRadius: 5,
    height: 40
  },
  lanContainer: {
    flex: 2,
    justifyContent: "flex-start",
    width: "100%",
    alignItems: "center"
  },
  lanConatiner: { display: "flex", flexDirection: "row", marginTop: 50 },
  select: {
    height: 40,
    width: "90%",
    marginBottom: 20,
    backgroundColor: "#3b4c69",
    color: "#6780aa",
    borderRadius: 5,
    paddingLeft: 10,
    borderRadius: 5
  },
  selectOuter: {
    width: "60%",
    borderRadius: 5
  }
});

export default styles;
