import React, { Component } from "react";
import styles from "./styles";
import {
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Image,
  Dimensions, Animated, Keyboard, KeyboardAvoidingView
} from "react-native";
import { login, reset } from "../actions/login";
import { connect } from "react-redux";
const DEVICE_WIDTH = Dimensions.get("window").width;
const DEVICE_HEIGHT = Dimensions.get("window").height;
import { Icon } from 'react-native-elements'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      valid: true,
      secureTextEntry: true
    };
    this.imageHeight = new Animated.Value(100);
  }

  componentWillMount() {
    this.keyboardWillShowSub = Keyboard.addListener('keyboardWillShow', this.keyboardWillShow);
    this.keyboardWillHideSub = Keyboard.addListener('keyboardWillHide', this.keyboardWillHide);
  }

  componentDidMount() {
    this.props.reset();
    if (this.props.loggedIn) {
      this.props.navigation.navigate("Language");
    }
  }

  componentDidUpdate(nextProps) {
    if (nextProps.loggedIn !== this.props.loggedIn && this.props.loggedIn) {
      this.props.navigation.navigate("Language");
    }
  }

  keyboardWillShow = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: 40,
    }).start();
  };

  keyboardWillHide = (event) => {
    Animated.timing(this.imageHeight, {
      duration: event.duration,
      toValue: 40,
    }).start();
  };

  submit = () => {
    this.props.reset()
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (this.state.username.length === 0 || this.state.password.length == 0) {
      this.setState({ valid: false });
    } else if (reg.test(this.state.username) === false) {
      this.setState({ valid: false });
    } else {
      this.setState({ valid: true });
      console.log(this.state.username.toLowerCase())
      this.props.login(this.state.username.toLowerCase(), this.state.password);
    }
  };

  render() {
    return (
      <KeyboardAvoidingView style={styles.loginConatiner} behavior="padding" >
        <View style={styles.logoConatiner}>
          <View>
            <Animated.Image source={require("./../../../assests/images/logo.png")} resizeMode="contain" style={{ width: 200, height: this.imageHeight }} />
          </View>
          <View style={{ paddingTop: 30, alignItems: "center" }}>
            <Text style={styles.loginText}>Login</Text>
          </View>
        </View>
        <View style={styles.textContainer}>
          <View style={styles.emailConatiner}>
            <View>
              <Image source={require("./../../../assests/images/mail-icon.png")} resizeMode="contain" style={{ width: 18, height: 18, margin: 10 }} />
            </View>
            <View style={styles.emailOuter}>
              <TextInput style={styles.emailInput} onChangeText={text => this.setState({ username: text })} value={this.state.username} placeholder="Enter Email" keyboardType="email-address" />
            </View>
          </View>
          <View style={styles.passwordConatier}>
            <View>
              <Image source={require("./../../../assests/images/password-icon.png")} resizeMode="contain" style={{ width: 18, height: 18, margin: 10 }} />
            </View>
            <View style={[styles.passwordOuter, { position: 'relative' }]}>
              <TextInput
                style={styles.passwordInput}
                onChangeText={text => this.setState({ password: text })}
                value={this.state.password}
                placeholder="Enter Password"
                secureTextEntry={this.state.secureTextEntry}
                mode="outlined"
              />
              <TouchableOpacity onPress={() => this.setState({ secureTextEntry: !this.state.secureTextEntry })}
                style={{ position: 'absolute', right: 26, top: 8 }}  >
                {this.state.secureTextEntry ? (<Icon name="visibility" size={24} color='#4a85c5' style={{ padding: 20 }} />) : (
                  <Icon name="visibility-off" color='#3660b7' size={24} style={{ padding: 20 }} />
                )}
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.forgotContainer} >
            <TouchableOpacity onPress={() => this.props.navigation.navigate("ForgotPassword")}>
              <Text style={styles.forgottext}>Forgotten password?</Text>
            </TouchableOpacity>
            {!this.state.valid ? (<Text style={styles.errorText}>Please enter all fields</Text>) : null}

            {this.props.login_error ? (<Text style={styles.errorText}>{this.props.error_message}</Text>) : null}

            {this.props.invalid_role ? (<Text style={styles.errorText}>Invalid Role</Text>) : null}

            {!this.props.licenceValid ? (<Text style={styles.errorText}> Licence Expired Please contact admin ! </Text>) : null}
          </View>
        </View>
        <View style={styles.buttonouter}>
          <TouchableOpacity style={styles.loginButton} onPress={() => this.submit()}>
            <Text style={styles.text}>Login</Text>
          </TouchableOpacity>
        </View>
        {/* </View> */}
      </KeyboardAvoidingView>
    );
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: state.loginReducer.loggedIn,
    login_error: state.loginReducer.login_error,
    invalid_role: state.loginReducer.invalid_role,
    error_message: state.loginReducer.error_message,
    licenceValid: state.homeReducer.licenceValid
  };
};

const mapDispatchToProps = {
  login,
  reset
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
