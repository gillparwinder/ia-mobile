import React, { Component } from "react";
import styles from "./styles";
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  TouchableHighlight,
  Picker,
  Modal
} from "react-native";
import { setLanguage } from "../actions/login";
import { connect } from "react-redux";
import { Icon } from "react-native-elements";
const Item = Picker.Item;

const languages = require("../../../assests/languages.json");
class Language extends Component {
  constructor(props) {
    super(props);
    this.state = {
      language: ""
    };
  }

  componentDidMount() {
    this.setState({
      language: this.props.language
    });
  }

  submit = () => {
    this.props.setLanguage(this.state.language);
    if (this.props.role == "GUEST_USER") {
      this.props.navigation.navigate("MemberEntity");
    }
    else if (this.props.role == "CENTRAL_SUPPORT_EXECUTIVE") {
      this.props.navigation.navigate("ListFieldExecutives");
    }
    else this.props.navigation.navigate("Home");
  };

  render() {
    return (
      <View style={styles.loginConatiner}>
        <View style={styles.logoConatiner}>
          <View>
            <Image
              source={require("./../../../assests/images/logo.png")}
              resizeMode="contain"
              style={{ width: 200, height: 100 }}
            />
          </View>
          <View style={{ paddingTop: 30, alignItems: "center" }}>
            <Text style={styles.loginText}>Default chat language</Text>
          </View>
        </View>
        <View style={styles.lanContainer}>
          <View style={styles.lanConatiner}>
            <View style={{ paddingLeft: 10, paddingRight: 10, paddingTop: 5 }}>
              <Icon
                name="chat-bubble-outline"
                type="material"
                color="#fff"
                size={25}
              />
            </View>
            <View style={styles.selectOuter}>
              {/* <View>
                <TouchableOpacity
                  style={{
                    height: 40,
                    width: "90%",
                    backgroundColor: "#3b4c69",
                    color: "#6780aa",
                    borderRadius: 5,
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "space-between"
                  }}
                  onPress={() => this.toggleModalVisible()}
                >
                  <Text style={{ color: "#ADADAD", paddingStart: 10 }}>
                    Select
                  </Text>
                  <View style={{ marginBottom: 12, right: 10 }}>
                    <Icon
                      name="sort-down"
                      type="font-awesome"
                      style={{
                        fontSize: 18,
                        color: "#ADADAD"
                      }}
                      color="#0a3c8e"
                    />
                  </View>
                </TouchableOpacity>
              </View> */}
              <View
                style={{
                  height: 40,
                  width: "90%",
                  backgroundColor: "#3b4c69",
                  color: "#6780aa",
                  borderRadius: 5,
                  justifyContent: "center"
                }}
              >
                <Picker
                  textStyle={{ color: "#6780aa", width: "100%" }}
                  style={{ color: "#6780aa" }}
                  mode="dialog"
                  selectedValue={this.state.language}
                  prompt="Select"
                  onValueChange={value => this.setState({ language: value })}
                  itemStyle={{ backgroundColor: "#eee" }}
                >
                  <Picker.Item label="Choose language" />
                  {languages.map((item, index) => (
                    <Picker.Item
                      key={index}
                      label={item.name}
                      value={item.code}
                    />
                  ))}
                </Picker>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.buttonouter}>
          <TouchableOpacity
            style={styles.loginButton}
            onPress={() => this.submit()}
          >
            <Text style={styles.text}>Save</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    language: state.loginReducer.language,
    role: state.loginReducer.role
  };
};

const mapDispatchToProps = {
  setLanguage
};

export default connect(mapStateToProps, mapDispatchToProps)(Language);
