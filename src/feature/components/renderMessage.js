import React, { Component } from 'react'
import {
    StyleSheet,
    Dimensions,
    View,
    Text,
    Image
} from 'react-native'
const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height

const RenderMessage = props => {
    return (
        <View key={props.index} style={[styles.bubbleOuter, { alignItems: props.item.author === props.user_id ? "flex-end" : "flex-start" }]} >
            {props.item.message_type == 'image' ?
                <View style={styles.imgWrap}>
                    <Image source={{ uri: props.item.payload }} resizeMode="contain" style={styles.imgScreenshot} />
                </View>
                :
                <View style={[styles.bubbleInner, { backgroundColor: props.item.author === props.user_id ? "#0000ff" : "#fff" }]} >
                    <Text style={{ padding: 10, color: props.item.author === props.user_id ? "#fff" : "#000" }} >
                        {props.item.payload}
                    </Text>
                </View>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    bubbleOuter: {
        width: DEVICE_WIDTH,
        display: 'flex'
    },
    bubbleInner: {
        width: DEVICE_WIDTH - 50,
        borderRadius: 50,
        borderWidth: 2,
        borderColor: '#eee',
        margin: 5,
        marginRight: 20
    },
    imgWrap: {
        borderWidth: 1,
        borderColor: "#0000ff",
        borderRadius: 5,
        marginRight: 30,
        marginLeft: 30,
        paddingBottom: 5,
        paddingTop: 5
    },
    imgScreenshot: {
        width: 100,
        height: 100,
        resizeMode: 'contain'
    }
});

export default RenderMessage