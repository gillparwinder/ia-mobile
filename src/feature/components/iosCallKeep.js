import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, NetInfo } from 'react-native'
import {
    saveCurrentCall, registerFCMToken, setCallInActive,
    ignoreCall,
    disconnectOneToOneSession
} from '../home/actions/actions'
import { connect } from 'react-redux'
import RNCallKeep from 'react-native-callkeep';
import VoipPushNotification from 'react-native-voip-push-notification';
import { getDeviceInformation } from '../../helpers/helper'
import firebase from "react-native-firebase";
import firebaseConfig from "../../helpers/firebaseHelp"
import logo from '../../assests/images/ionlogo.png'

RNCallKeep.setup({
    ios: {
        appName: 'IonAssist',
        imageName: '40'
    }
}).then(accepted => {
    console.log("accepted");
});

class IOSCallKeep extends Component {
    constructor(props) {
        super(props)
        this.state = {
            callUUID: '',
            firebaseKey: '',
            from_role: '',
            requestedUser: '',
            sessionId: '',
            title: '',
            userId: '',
            isCallAttended: false,
            callEndedByOtherParty: false
        }
    }

    componentWillMount() {
        VoipPushNotification.requestPermissions();

        VoipPushNotification.addEventListener('notification', (notification) => {
            if (VoipPushNotification.wakeupByPush) {
                VoipPushNotification.wakeupByPush = false;
            }
            // --- optionally, if you `addCompletionHandler` from the native side, once you have done the js jobs to initiate a call, call `completion()`
            // VoipPushNotification.onVoipNotificationCompleted(notification.getData().uuid);
            // VoipPushNotification.presentLocalNotification({
            //     alertBody: "hello! " + notification.getMessage()

            // });
        });
    }

    componentDidMount() {
        let firebaseUrl = ''
        this.props.initiatedBy == 'CentralSupport' ?
            firebaseUrl = "ionassist-root/sessions/" + this.props.member_entity :
            firebaseUrl = "ionassist-root/one_to_one/" + this.props.member_entity;

        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }

        sessionRef = firebase
            .database()
            .ref(firebaseUrl);

        sessionRef.on("child_changed", snapshot => {
            if (snapshot.val().status == "DISCONNECTED" && snapshot.val().firebase_key == this.state.firebaseKey && snapshot.val().terminated_user_name != this.props.user_name) {
                //if (!this.state.isCallAttended) {
                this.setState({ callEndedByOtherParty: true });
                RNCallKeep.endCall(this.state.callUUID);
                //}
            }
        });

        VoipPushNotification.registerVoipToken(); // --- required
        VoipPushNotification.addEventListener('register', (token) => {
            let deviceInfo = getDeviceInformation()
            this.props.registerFCMToken(deviceInfo.device_uid, token);
            //alert("token:" + token);
        },
            (error) => {
                alert("some error")
            });

        // RNCallKeep.addEventListener('didReceiveStartCallAction', this.onNativeCall);
        RNCallKeep.addEventListener('answerCall', ({ callUUID }) => { this.onAnswerCallAction(callUUID) });
        RNCallKeep.addEventListener('endCall', ({ callUUID }) => { this.onEndCallAction(callUUID) });
        RNCallKeep.addEventListener('didDisplayIncomingCall', this.onIncomingCallDisplayed);
        // RNCallKeep.addEventListener('didPerformSetMutedCallAction', this.onToggleMute);
        // RNCallKeep.addEventListener('didPerformDTMFAction', this.onDTMF);
    }

    onAnswerCallAction(callUUID) {
        this.setState({ isCallAttended: true });
        this.props.initiatedBy == 'CentralSupport' ?
            this.props.navigate("ReceiveCall", { sessionId: this.state.sessionId, firebaseKey: this.state.firebaseKey }) :
            this.props.navigate("AttendCallFromSupport", { sessionId: this.state.sessionId, firebaseKey: this.state.firebaseKey })
    }

    onEndCallAction(callUUID) {
        if (!this.state.isCallAttended && !this.state.callEndedByOtherParty) {
            this.props.initiatedBy == 'CentralSupport' ?
                this.props.ignoreCall(this.state.firebaseKey) : this.props.disconnectOneToOneSession(this.state.firebaseKey);
        }
        this.setState({ isCallAttended: false });
    }

    onIncomingCallDisplayed = ({ error, callUUID, handle, localizedCallerName, hasVideo, fromPushKit, payload }) => {
        // You will get this event after RNCallKeep finishes showing incoming call UI
        // You can check if there was an error while displaying
        this.setState({ callEndedByOtherParty: false });
        this.setState({
            callUUID: callUUID,
            firebaseKey: payload.firebaseKey,
            from_role: payload.from_role,
            requestedUser: payload.requestedUser,
            sessionId: payload.sessionId,
            title: payload.title,
            userId: payload.userId,
            isCallAttended: false
        })

        this.props.saveCurrentCall(callUUID);
    };

    render() { return <View></View> }
}

const styles = StyleSheet.create({
})

const mapStateToProps = state => {
    return {
        member_entities: state.loginReducer.member_entities,
        member_entity: state.loginReducer.member_entity,
        loggedIn: state.loginReducer.loggedIn,
        user_name: state.loginReducer.user_name,
        licenceValid: state.homeReducer.licenceValid,
        role: state.loginReducer.role
    }
}

const mapDispatchToProps = {
    saveCurrentCall,
    registerFCMToken,
    setCallInActive,
    ignoreCall,
    disconnectOneToOneSession
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IOSCallKeep)

