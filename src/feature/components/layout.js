import React, { Component } from 'react'
import { View, ImageBackground, StyleSheet } from 'react-native'

const Layout = props => {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require('../../assests/images/customer-support-banner.jpg')}
        resizeMode='cover'
        style={styles.imgBackground}
      >
        {props.children}
      </ImageBackground>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  imgBackground: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1
  },
  headerWrap: {
    // flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%'
  },
  iconLogo: {
    width: 40,
    height: 40,
    margin: 20
  },
  iconMenu: {
    width: 40,
    height: 40,
    position: 'absolute',
    right: 0,
    margin: 20
  }
})

export default Layout
