import React, { Component } from 'react'
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Modal,
  TouchableHighlight,
  TouchableWithoutFeedback,
  StyleSheet,
  Image
} from 'react-native'
import {
  logout,
  resetAccesToken,
  setMemberEntity
} from '../login/actions/login'
import { connect } from 'react-redux'
import iconOk from '../../assests/images/ok.png'
const DEVICE_WIDTH = Dimensions.get('window').width
const DEVICE_HEIGHT = Dimensions.get('window').height

class ModalMenu extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedEntity: '',
      onExpand: false
    }
  }

  componentWillMount() {
    this.props.member_entities.map((item, index) => {
      if (item.entity_id == this.props.member_entity) {
        this.setState({ selectedEntity: item.name })
      }
    })
  }

  componentDidUpdate(nextProps) {
    if (nextProps.loggedIn !== this.props.loggedIn) {
      if (!this.props.loggedIn) {
        this.props.navigate("Login");
      }
    }

    if (nextProps.licenceValid !== this.props.licenceValid) {
      if (!this.props.licenceValid) {
        this.props.resetLogin();
        this.props.navigate("Login");
      }
    }
  }

  componentDidMount() { }

  language = () => {
    this.props.setModalVisible(false)
    this.props.navigate('Language')
  }

  resetPassword = () => {
    this.props.setModalVisible(false)
    this.props.navigate('PasswordReset')
  }

  hideModal = () => {
    this.props.setModalVisible(false)
    this.setState({ onExpand: false })
  }

  setMemberEntity = entityId => {
    this.props.setMemberEntity(entityId)
    this.setState({ onExpand: false })
    this.props.resetAccesToken()
  }

  render() {
    return (
      <Modal
        animationType='fade'
        transparent
        visible={this.props.showModal}
        onRequestClose={() => this.hideModal()}
      >
        <TouchableOpacity
          style={{ width: DEVICE_WIDTH, height: DEVICE_HEIGHT }}
          activeOpacity={1}
          onPressOut={() => {
            this.hideModal()
          }}
        >
          <ScrollView
            directionalLockEnabled
            contentContainerStyle={styles.scrollModal}
            style={{ marginTop: 60 }}
          >
            <TouchableWithoutFeedback>
              <View style={styles.modalContainer}>
                <View style={styles.modalOuter}>

                  {/* {this.props.role == 'GUEST_USER' ?
                    <TouchableHighlight
                      style={styles.clientName}
                      onPress={() =>
                        this.setState({ onExpand: !this.state.onExpand })
                      }
                    >
                      <View>
                        {this.props.member_entities.map((item, index) => {
                          return item.entity_id == this.props.member_entity ? (
                            <Text key={index} style={styles.txtClient}>
                              {item.name}
                            </Text>
                          ) : null
                        })}
                      </View>
                    </TouchableHighlight>
                    : null}

                  {this.state.onExpand ? (
                    <View style={styles.clientsWrap}>
                      {this.props.member_entities.map((item, index) => {
                        return (
                          <TouchableHighlight
                            key={index}
                            style={styles.selectClient}
                            onPress={() => {
                              this.setMemberEntity(item.entity_id)
                            }}
                          >
                            <View>
                              <Text style={styles.txtClient}>{item.name}</Text>
                              {item.entity_id == this.props.member_entity ? (
                                <Image
                                  source={iconOk}
                                  resizeMode='contain'
                                  style={styles.okIcon}
                                />
                              ) : null}
                            </View>
                          </TouchableHighlight>
                        )
                      })}
                    </View>
                  ) : null} */}

                  <TouchableHighlight
                    underlayColor='#629cff'
                    style={styles.settings}
                    onPress={() => this.language()}
                  >
                    <Text style={{ paddingLeft: 15 }}>Language Settings</Text>
                  </TouchableHighlight>

                  <TouchableHighlight
                    onPress={() => this.resetPassword()}
                    underlayColor='#629cff'
                    style={styles.settings}
                  >
                    <Text style={{ paddingLeft: 15 }}>Reset Password</Text>
                  </TouchableHighlight>

                  <TouchableHighlight
                    onPress={() => this.props.logout()}
                    underlayColor='#629cff'
                    style={styles.settings}
                  >
                    <Text style={{ paddingLeft: 15 }}>Logout</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </ScrollView>
        </TouchableOpacity>
      </Modal>
    )
  }
}

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalOuter: {
    width: '90%',
    backgroundColor: '#fff',
    display: 'flex',
    flexDirection: 'column',
    borderRadius: 4
  },
  settings: {
    flex: 1,
    padding: 10,
    width: '100%'
  },
  clientName: {
    backgroundColor: '#010101',
    padding: 10,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4
  },
  txtClient: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  clientsWrap: {
    padding: 20
  },
  selectClient: {
    backgroundColor: '#3b4c69',
    marginBottom: 15,
    borderRadius: 5,
    padding: 10
  },
  okIcon: {
    width: 20,
    height: 20,
    position: 'absolute',
    right: 10
  }
})

const mapStateToProps = state => {
  return {
    member_entities: state.loginReducer.member_entities,
    member_entity: state.loginReducer.member_entity,
    loggedIn: state.loginReducer.loggedIn,
    licenceValid: state.homeReducer.licenceValid,
    role: state.loginReducer.role
  }
}

const mapDispatchToProps = {
  resetAccesToken,
  setMemberEntity,
  logout
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModalMenu)
