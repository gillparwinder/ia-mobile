import React, { Component } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { Icon } from 'react-native-elements'
import ModalMenu from './modalMenu'

class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      logoutModal: false,
      location: false,
      loader: false
    }
  }

  setModalVisible = visible => {
    this.setState({ logoutModal: visible })
  }

  render() {
    return (
      <View style={styles.headerWrap}>
        <Image
          source={require('../../assests/images/logo-transparent.png')}
          resizeMode='contain'
          style={styles.iconLogo}
        />
        <TouchableOpacity
          style={styles.iconMenu}
          onPress={() => this.setModalVisible(!this.state.logoutModal)}
        >
          {this.state.logoutModal ? (
            <Icon name='close' type='evilicon' color='red' size={40} />
          ) : (
              <Icon name='navicon' type='evilicon' color='#fff' size={40} />
            )}
        </TouchableOpacity>
        <ModalMenu
          showModal={this.state.logoutModal}
          setModalVisible={this.setModalVisible}
          navigate={this.props.navigate}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  headerWrap: {
    // flex: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    height: 60,
    alignItems: 'center',
    paddingRight: 15,
    paddingLeft: 15,
    paddingTop: 15
  },
  iconLogo: {
    width: 40,
    height: 40,
    //  margin: 20
  },
  iconMenu: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center'
    // position: 'absolute',
    // right: 0,
    //  margin: 20,
  }
})

export default Header
