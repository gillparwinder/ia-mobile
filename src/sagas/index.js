import { all, fork } from 'redux-saga/effects';

import { watchHome } from '../feature/home/sagas/homeSaga';
import { watchLogin } from '../feature/login/sagas/loginSaga';
export default function* rootSaga() {
    yield all([
        watchHome(),
        watchLogin()
    ])
}