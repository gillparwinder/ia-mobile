export const getRefreshToken = state => state.loginReducer.refresh_token;
export const getAccessToken = state => state.loginReducer.auth_token;
export const member_entity = state => state.loginReducer.member_entity;
