import { call, put, select } from "redux-saga/effects";
import { getAccessToken } from "./../feature/login/api/api.login";
import * as sagaSelectors from "./selectors";
import * as types from "../types/types";

function headers() {
  return {
    Accept: "application/json",
    "Content-Type": "application/json",
    dataType: "json"
  };
}

export function* callAPI(api, payload, failureType) {
  try {
    let accessToken = yield select(sagaSelectors.getAccessToken);
    const header = headers();
    if (accessToken) {
      header["Authorization"] = "bearer " + accessToken;
    }
    let response = yield call(api, payload, header);
    if (
      response &&
      response.data.error &&
      response.data.error.status &&
      response.data.error.status == 401
    ) {
      //The token might have expired and hence requesting for a new access token for the team

      const refreshToken = yield select(sagaSelectors.getRefreshToken);
      const member_entity = yield select(sagaSelectors.member_entity);

      const accessTokenResulut = yield call(
        getAccessToken,
        {
          refresh_token: refreshToken,
          member_entity: member_entity
        },
        header
      );

      yield put({
        type: types.ACCESS_TOKEN_SUCCESS,
        payload: accessTokenResulut.data
      });

      console.log("access token  " + JSON.stringify(accessTokenResulut));
      header["Authorization"] =
        "bearer " + accessTokenResulut.data["access_token"];
      response = yield call(api, payload, header);
    }

    if (!response.status) {
      yield put({
        type: failureType,
        payload: {
          error_message:
            response.data && response.data.message ? response.data.message : ""
        }
      });
      return null;
    }
    return response;
  } catch (e) {
    console.log(e);
    yield put({ type: failureType });
  }
  return null;
}
