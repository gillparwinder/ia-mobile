#!/usr/bin/env bash

set -x
echo "Uninstalling the node version 6.x"
brew uninstall node@6

NODE_VERSION="10.16.0"
echo "Installing node v${NODE_VERSION}"

curl "https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}.pkg" > "$HOME/Downloads/node-installer.pkg"
sudo installer -store -pkg "$HOME/Downloads/node-installer.pkg" -target "/"

echo "Completed installing node v${NODE_VERSION}"


echo "The current BUILD_ENV is $BUILD_ENV"

if [ "$BUILD_ENV" == "integration" ]; then    
    echo "Copying integration environment variables to .env"
    echo ".env.integration" > /tmp/envfile
    cp .env.integration .env
elif [ "$BUILD_ENV" == "release" ]; then    
    echo "Copying release environment variables to .env"
    echo ".env.release" > /tmp/envfile
    cp .env.release .env
elif [ "$BUILD_ENV" == "staging" ]; then
    echo "Copying staging environment variables to .env"
    echo ".env.staging" > /tmp/envfile
    cp .env.staging .env
elif [ "$BUILD_ENV" == "development" ]; then
    echo "Copying staging environment variables to .env"
    echo ".env.development" > /tmp/envfile
    cp .env.staging .env
else
    echo "No valid configurations."
fi


echo "calling script to generate react native configs" 
./ios/react-native-config-gen.sh
echo "Completed generating the configurations"