import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import { persistStore } from "redux-persist";
import rootSaga from "./src/sagas/index";
import reducer from "./src/reducers/index";

import { createStore, applyMiddleware, compose } from "redux";

const loggerMiddleware = createLogger({
  predicate: (getState, action) => __DEV__
});

const sagaMiddleware = createSagaMiddleware();

const enhancer = compose(applyMiddleware(loggerMiddleware, sagaMiddleware));

const store = createStore(reducer, enhancer);

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export default function configureStore() {
  return { store, persistor };
}
